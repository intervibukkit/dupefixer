package ru.intervi.dupefixer.dupe;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import ru.intervi.dupefixer.Main;

/**
 * фикс размножения больших цветков костной мукой (НЕ дюп)
 */
public class BigFlowers {
	public BigFlowers(Main m) {
		main = m;
	}
	private Main main;
	
	public void onInteract(PlayerInteractEvent event) {
		if (!main.conf.enable || !main.conf.dupefix || !main.conf.bigflower) return;
		Player player = event.getPlayer();
		if (player.hasPermission("dupefixer.exemtp")) return;
		ItemStack item = event.getItem();
		
		if (event.getClickedBlock().getType().equals(Material.DOUBLE_PLANT) && item.getType().equals(Material.INK_SACK)) {
			event.setCancelled(true);
			String cords = main.getLoc(player.getLocation());
			String name = player.getName();
			if (main.conf.warn) player.sendMessage(main.conf.warnmess);
			main.info(main.repAll(main.mess.flInter, "%name%", name, "%cords%", cords));
		}
	}
}