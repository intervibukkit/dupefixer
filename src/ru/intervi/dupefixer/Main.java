package ru.intervi.dupefixer;

import org.bukkit.Location;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.command.CommandSender;
import org.bukkit.command.Command;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerJoinEvent;

import ru.intervi.littleconfig.utils.Utils;
import ru.intervi.littleconfig.utils.EasyLogger;
import ru.intervi.dupefixer.checks.XRay.XStat;
import ru.intervi.dupefixer.bugs.AnimalsTP;
import ru.intervi.dupefixer.spy.CMDUtils;
import ru.intervi.dupefixer.bugs.ChunksGC;

import java.io.File;
import java.util.HashSet;
import java.util.Timer;

/**
 * главный класс плагина
 */
public class Main extends JavaPlugin implements Listener {
	private EasyLogger info = new EasyLogger( //логгер для инфо сообщений
			"[DupeFixer]",
			new File((Utils.getFolderPath(this.getClass()) + File.separator + "DupeFixer" + File.separator + "info.log"))
			);
	protected EasyLogger error = new EasyLogger( //логгер для сообщений об ошибках
			"[DupeFixer]",
			new File((Utils.getFolderPath(this.getClass()) + File.separator + "DupeFixer" + File.separator + "error.log"))
			);
	
	public Config conf = new Config(this); //грузим конфиг
	public Mess mess = new Mess(this); //локализация плагина
	public Events ev = new Events(this); //обработчик ивентов
	private HashSet<String> mute = new HashSet<String>(); //бд с теми, кого не оповещать
	private String nv = null; //есть ли новая версия (ссылка)
	private Timer timer = null;
	private AnimalsTP antp = new AnimalsTP(this); //телепортатор животных
	private CMDUtils cutils = new CMDUtils(this); //полезные утилиты
	private ChunksGC cgc = new ChunksGC(this); //отгрузчик чанков
	
	void setUpdate(String link) { //найдено обновление
		if (link == null) return;
		nv = link;
		info(repAll(mess.update, "%link%", nv));
	}
	
	private void startTimer() {
		//обновления проверяются через 30сек после старта и потом каждый час
		timer = new Timer();
		timer.schedule(new UpdateChecker(this), 30*1000, 60*60*60*1000);
	}
	
	private void stopTimer() {
		if (timer != null) {
			timer.cancel();
			timer = null;
		}
	}
	
	public void info(String mess) { //запись инфо сообщения
		if (conf.duptocons) {
			getLogger().info(("[DupeFixer] " + mess));
			if (nv != null) getLogger().info(("[DupeFixer]" + repAll(this.mess.update, "%link%", nv)));
		}
		if (conf.dupetogame) {
			for (Player pl : Bukkit.getOnlinePlayers()) {
				if (pl.hasPermission("dupefixer.notify")) {
					if (mute.contains(pl.getName())) return; //пропуск выключенных
					pl.sendMessage("[DupeFixer] " + mess);
				}
			}
		}
		if (conf.dupetolog) info.info(mess);
	}
	public void error(Exception e) { //запись крашей
		if (conf.dupetolog) error.error(e);
		getLogger().warning(mess.erlog);
		if (nv != null) getLogger().info(("[DupeFixer]" + repAll(mess.update, "%link%", nv)));
	}
	public void error(String mess) { //запись ошибок
		if (conf.dupetolog) error.error(mess);
		getLogger().warning(mess);
		if (nv != null) getLogger().info(("[DupeFixer]" + repAll(this.mess.update, "%link%", nv)));
	}
	
	@Override
	public void onLoad() { //настройка логгеров
		getLogger().info("[DupeFixer] Инициализация...");
		info.offLog();
		error.offLog();
	}
	
	@Override
	public void onEnable() {
		getLogger().info("[DupeFixer] <--------------------->");
		getLogger().info("[DupeFixer] Старт...");
		getServer().getPluginManager().registerEvents(ev, this);
		if (conf.enable && conf.update) startTimer();
		if (conf.enable && conf.bugs && conf.animalstp) antp.startTimer();
		if (conf.enable && conf.bugs && conf.antifall) ev.af.startTimer();
		if (conf.enable && conf.bugs && conf.chunksgc) cgc.startTimer();
		getLogger().info("[DupeFixer] Created by InterVi");
		getLogger().info("[DupeFixer] <--------------------->");
	}
	
	@Override
	public void onDisable() { //остановка таймеров и очистка списков
		getLogger().info("[DupeFixer] Выключение...");
		stopTimer();
		antp.stopTimer();
		ev.xr.stopTimer();
		ev.af.stopTimer();
		cgc.stopTimer();
		clearAll();
	}
	
	public String getLoc(Location loc) { //получить локацию в виде строки
        return loc.getWorld().getName() + ": " + loc.getBlockX() + " " + loc.getBlockY() + " " + loc.getBlockZ();
    }
	
	/**
	 * быстрая замена элементов строки
	 * @param args
	 * 0 - строка
	 * 1 - что заменить
	 * 2 - на что заменить
	 * далее по аналогии
	 * @return обработанная строка
	 */
	public String repAll(String... args) {
		if (args.length < 1) return null;
		if (args.length < 3) return args[0];
		String result = args[0];
		for (int i = 1; i < args.length; i++) {
			if ((i+1) >= args.length) break;
			result = result.replaceAll(args[i], args[i+1]);
			i++;
		}
		return result;
	}
	
	void onJoin(PlayerJoinEvent event) { //оповещение о новой версии
		if (nv == null) return;
		Player player = event.getPlayer();
		if (player.isOp()) {
			player.sendMessage(repAll(mess.update, "%link%", nv));
		}
	}
	
	private void clearAll() { //очистка данных
		ev.xr.clear();
		ev.de.clear();
		ev.fc.clear();
		ev.af.clear();
		ev.sh.clear();
		ev.lt.clear();
		ev.tpv.clear();
		ev.wh.clear();
		mute.clear();
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (args == null || args.length == 0) {
			sender.sendMessage("[DupeFixer] <--------------------->");
			sender.sendMessage(mess.coms);
			sender.sendMessage("[DupeFixer] Created by InterVi");
			sender.sendMessage("[DupeFixer] <--------------------->");
			return true;
		}
		switch(args[0].toLowerCase()) {
		case "reload":
			if (sender.hasPermission("dupefixer.reload")) {
				conf.load();
				mess.load();
				if (conf.enable && conf.checks && conf.xray) {
					ev.xr.stopTimer();
					ev.xr.startTimer();
				} else ev.xr.stopTimer();
				if (conf.enable && conf.update) {
					stopTimer();
					startTimer();
				} else stopTimer();
				if (conf.enable && conf.bugs && conf.animalstp) {
					antp.stopTimer();
					antp.startTimer();
				} else antp.stopTimer();
				if (conf.enable && conf.bugs && conf.antifall) {
					ev.af.stopTimer();
					ev.af.startTimer();
				} else ev.af.startTimer();
				if (conf.enable && conf.bugs && conf.chunksgc) {
					cgc.stopTimer();
					cgc.startTimer();
				} else cgc.startTimer();
				sender.sendMessage(mess.creload);
			} else sender.sendMessage(mess.noperm);
			break;
		case "clear":
			if (sender.hasPermission("dupefixer.clear")) {
				clearAll();
				sender.sendMessage(mess.memclean);
			} else sender.sendMessage(mess.noperm);
			break;
		case "xstat":
			if (sender.hasPermission("dupefixer.xstat")) {
				if (args.length >= 2) {
					if (conf.enable && conf.checks && conf.xray) {
						Player player = Bukkit.getPlayer(args[1]);
						if (player != null) {
							XStat stat = ev.xr.getStat(player.getUniqueId());
							if (stat != null) {
								sender.sendMessage("[DupeFixer] <--------------------->");
								String send[] = mess.xstat;
								if (send.length == 4) {
									send[0] = repAll(send[0], "%name%", player.getName());
									send[1] = repAll(send[1], "%norm0%", String.valueOf(stat.NORM[0]), "%norm1%",  String.valueOf(stat.NORM[1]));
									send[2] = repAll(send[2], "%norm2%", String.valueOf(stat.NORM[2]));
									send[3] = repAll(send[3], "%cheat%", repAll(String.valueOf(stat.CHEATER), "true", "да", "false", "нет"));
								}
								sender.sendMessage(send);
								sender.sendMessage("[DupeFixer] <--------------------->");
							} else sender.sendMessage(mess.nodata);
						} else sender.sendMessage(mess.badname);
					} else sender.sendMessage(mess.modoff);
				} else sender.sendMessage(mess.badname);
			} else sender.sendMessage(mess.noperm);
			break;
		case "update":
			if (sender.hasPermission("dupefixer.update")) {
				sender.sendMessage(mess.upstart);
				Update up = new Update(this, sender);
				up.start();
			} else sender.sendMessage(mess.noperm);
			break;
		case "report":
			if (sender.hasPermission("dupefixer.notify")) {
				mute.add(sender.getName());
				sender.sendMessage(mess.non);
			} else sender.sendMessage(mess.noperm);
			break;
		case "noreport":
			if (sender.hasPermission("dupefixer.notify")) {
				mute.remove(sender.getName());
				sender.sendMessage(mess.noff);
			} else sender.sendMessage(mess.noperm);
			break;
		case "uuid":
			if (args.length >= 2) {
				sender.sendMessage(cutils.getUUID(args[1], sender));
			} else sender.sendMessage(mess.badname);
			break;
		case "name":
			if (args.length >= 2) {
				sender.sendMessage(cutils.getName(args[1], sender));
			} else sender.sendMessage(mess.badname);
			break;
		case "ltclear":
			if (sender.hasPermission("dupefixer.ltclear")) {
				ev.lt.clearList();
				sender.sendMessage(mess.ltclear);
			} else sender.sendMessage(mess.noperm);
			break;
		case "cgc":
			if (sender.hasPermission("dupefixer.cgc")) {
				if (!conf.enable || !conf.bugs || !conf.chunksgc) {
					sender.sendMessage(mess.modoff);
					return true;
				}
				sender.sendMessage(repAll(mess.cgc, "%chunks%", String.valueOf(cgc.gc())));
				if (conf.sysgc) Runtime.getRuntime().gc();
			} else sender.sendMessage(mess.noperm);
			break;
		case "online":
			if (sender.hasPermission("dupefixer.online")) {
				if (args.length < 2) {
					sender.sendMessage(mess.badname);
					break;
				}
				Player player = Bukkit.getPlayer(args[1]);
				if (player != null) sender.sendMessage(cutils.getOnline(player.getUniqueId()));
			} else sender.sendMessage(mess.noperm);
			break;
		default:
				return false;
		}
		return true;
	}
}