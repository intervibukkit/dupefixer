package ru.intervi.dupefixer.spy;

import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.Material;

import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.Date;
import java.util.Map.Entry;

import ru.intervi.dupefixer.Main;

/**
 * отслеживание раздач и высокой концентрации ресурсов
 */
public class Share {
	public Share(Main m) {
		main = m;
	}
	
	private Main main;
	private ConcurrentHashMap<UUID, Date> map = new ConcurrentHashMap<UUID, Date>(); //для антифлуда
	
	public void onPickup(PlayerPickupItemEvent event) {
		if (!main.conf.enable || !main.conf.spy || !main.conf.share) return;
		Player player = event.getPlayer();
		if (player.hasPermission("dupefixer.exempt") || player.hasPermission("dupefixer.nospy")) return;
		ItemStack item = event.getItem().getItemStack();
		if (item.getAmount() < main.conf.samount) return;
		if (check(item)) { //если дорогой предмет
			main.info(main.repAll(main.mess.spickup, "%name%", player.getName(),
					"%itemname%", item.getType().toString(), "%num%", String.valueOf(item.getAmount()),
					"%loc%", main.getLoc(event.getItem().getLocation())));
		}
	}
	
	public void onDrop(PlayerDropItemEvent event) {
		if (!main.conf.enable || !main.conf.spy || !main.conf.share) return;
		Player player = event.getPlayer();
		if (player.hasPermission("dupefixer.exempt") || player.hasPermission("dupefixer.nospy")) return;
		ItemStack item = event.getItemDrop().getItemStack();
		if (item.getAmount() < main.conf.samount) return;
		if (check(item)) { //если дорогой предмет
			main.info(main.repAll(main.mess.sdrop, "%name%", player.getName(),
					"%itemname%", item.getType().toString(), "%num%", String.valueOf(item.getAmount()),
					"%loc%", main.getLoc(event.getItemDrop().getLocation())));
		}
	}
	
	/**
	 * проверка на наличие в списке
	 * @param item предмет
	 * @return true если есть; false если нету
	 */
	private boolean check(ItemStack item) {
		String itemname = item.getType().toString();
		for (String iname : main.conf.sitems) {
			if (iname.equalsIgnoreCase(itemname)) {
				return true;
			}
		}
		return false;
	}
	
	public void onClick(InventoryClickEvent event) {
		if (!main.conf.enable || !main.conf.spy || !main.conf.share) return;
		Player player = (Player) event.getWhoClicked();
		UUID uuid = player.getUniqueId();
		if (player.hasPermission("dupefixer.exempt") || player.hasPermission("dupefixer.nospy")) return;
		Date d = map.get(uuid);
		long sec = 0;
		if (d != null) sec = (System.currentTimeMillis() - d.getTime()) / 1000; //антифлуд
		if ((event.isLeftClick() || event.isRightClick() || event.isShiftClick()) && sec >= main.conf.scwaitsec) {
			ItemStack cursor = event.getCursor();
			ConcurrentHashMap<Material, Integer> hash = new ConcurrentHashMap<Material, Integer>(); //все дорогие предметы
			if (check(cursor)) { //проверка курсора
				hash.put(cursor.getType(), new Integer(cursor.getAmount()));
			}
			for (byte i = 0; i <= 3; i++) { //проверка инвентарей
				ItemStack items[] = player.getInventory().getContents(); //инвентарь игрока
				switch(i) {
				case 1:
					items = player.getEnderChest().getContents(); //эндер сундук
					break;
				case 2:
					items = player.getInventory().getArmorContents(); //броня
					break;
				case 3:
					items = event.getInventory().getContents(); //открытый инвентарь
				}
				for (ItemStack item : items) { //добавление нужного в список, кол-во суммируется
					if (item == null) continue;
					if (check(item)) {
						if (hash.containsKey(item.getType())) {
							int old = hash.get(item.getType());
							hash.replace(item.getType(), new Integer(item.getAmount() + old));
						}
						else hash.put(item.getType(), new Integer(item.getAmount()));
					}
				}
			}
			String items = "";
			for (Entry<Material, Integer> set : hash.entrySet()) { //заполнение информации
				if (set.getValue().intValue() >= main.conf.scamount)
					items += set.getKey().toString() + '(' + set.getValue().toString() + "), ";
			}
			if (!items.isEmpty()) { //вывод информации
				items = items.substring(0, (items.length()-2));
				main.info(main.repAll(main.mess.sclick, "%name%", player.getName(), "%items%", items,
						"%loc%", main.getLoc(player.getLocation())));
			}
		}
		if (d != null) map.replace(uuid, new Date()); else map.put(uuid, new Date());
	}
	
	public void onQuit(PlayerQuitEvent event) { //очистка списка для антифлуда
		map.remove(event.getPlayer().getUniqueId());
	}
	
	public void clear() {map.clear();}
}