package ru.intervi.dupefixer.bugs;

import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.entity.Player;
import org.bukkit.Bukkit;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.scheduler.BukkitTask;

import java.util.ArrayList;
import java.util.UUID;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Iterator;

import ru.intervi.dupefixer.Main;

/**
 * анти-застревание в блоках при телепортации
 */
public class AntiFall {
	public AntiFall(Main m) {
		main = m;
	}
	
	/**
	 * класс с данными для обработки списков
	 */
	class Data {
		Data(Player p, Location l, long s) {
			player = p;
			loc = l;
			stamp = s;
		}
		Data(Player p, Location l) {
			player = p;
			loc = l;
		}
		
		Player player; //игрок
		Location loc; //локация телепортации
		long stamp; //время добавления в список
	}
	
	private Main main;
	private ArrayList<Data> ych = new ArrayList<Data>(); //список для проверки на падение
	private ArrayList<Data> wait = new ArrayList<Data>(); //список для задержки телепортации
	private ArrayList<UUID> check = new ArrayList<UUID>(); //список телепортированных
	private Timer timer = null;
	private BukkitTask task = null;
	
	public void onTeleport(PlayerTeleportEvent event) {
		if (!main.conf.enable || !main.conf.bugs || !main.conf.antifall) return;
		if (main.conf.onlycause) { //обработка только заданных причин телепортации
			String cause = event.getCause().toString();
			boolean r = true;
			for (String c : main.conf.tpcause) {
				if (cause.equalsIgnoreCase(c)) {
					r = false;
					break;
				}
			}
			if (r) return;
		}
		Player player = event.getPlayer();
		Location loc = event.getTo();
		Location from = event.getFrom();
		if (player.getLastPlayed() == 0) return; //костыль, потому что при входе срабатывают два ивента сразу
		if (main.conf.falldist != 0 &&
				(loc.getWorld().getName().equals(from.getWorld().getName()) && loc.distance(from) <= main.conf.falldist))
			return; //проверка расстояния
		UUID uuid = player.getUniqueId();
		if (check.contains(uuid)) { //предотвращение вечного цикла
			check.remove(uuid);
			return;
		}
		if (main.conf.checkair) { //проверка на застревание
			if (!loc.getBlock().isEmpty()) {
				loc = getEmpty(loc);
				event.setTo(loc);
			}
		}
		if (main.conf.preload) { //предзагрузка чанка
			wait.add(new Data(player, loc));
			event.setCancelled(true);
		} else if (main.conf.timcheck) { //проверка падения по таймеру
			ych.add(new Data(player, loc, System.currentTimeMillis()));
		}
	}
	
	public void onJoin(PlayerJoinEvent event) {
		if (!main.conf.enable || !main.conf.bugs || !main.conf.antifall || !main.conf.joincheck) return;
		Player player = event.getPlayer();
		Location loc = player.getLocation();
		if (!loc.getBlock().isEmpty()) { //проверка, что игрок не застрял при входе
			loc = getEmpty(loc);
			player.teleport(loc);
		}
		if (main.conf.timcheck) ych.add(new Data(player, loc, System.currentTimeMillis()));
	}
	
	public void startTimer() {
		timer = new Timer();
		timer.schedule(new YCheck(), main.conf.tcint, main.conf.tcint);
		task = new WaitTP().runTaskTimer(Bukkit.getPluginManager().getPlugin("DupeFixer"), main.conf.plint, main.conf.plint);
	}
	
	public void stopTimer() {
		if (timer != null) {
			timer.cancel();
			timer = null;
		}
		if (task != null) {
			task.cancel();
			task = null;
		}
	}
	
	/**
	 * очистить списки
	 */
	public void clear() {
		wait.clear();
		ych.clear();
		check.clear();
	}
	
	/**
	 * проверить, твердый ли блок, пройдясь по списку из конфига
	 * @param block блок
	 * @return true - пустой; false - нет
	 */
	private boolean isEmpty(Block block) {
		String mat = block.getType().toString();
		for (String air : main.conf.airmat) {
			if (mat.equalsIgnoreCase(air)) return true;
		}
		return false;
	}
	
	/**
	 * получить первый верхний пустой блок
	 * @param loc локация старта поиска
	 * @return локация пустого блока
	 */
	private Location getEmpty(Location loc) {
		Block block = loc.getBlock();
		for (int i = 0; i <= loc.getWorld().getMaxHeight(); i++) {
			Block re = block.getRelative(0, i, 0);
			//проверка, что блок выше тоже пустой, иначе игрок задохнется
			if (isEmpty(re) && isEmpty(re.getRelative(0, (i+1), 0))) return re.getLocation();
		}
		return loc;
	}
	
	/**
	 * телепортатор игроков на заданные локации
	 */
	private class JobTP extends BukkitRunnable {
		JobTP(List<Data> d) {
			data = d;
		}
		
		private List<Data> data;
		
		@Override
		public void run() {
			for (Data d : data) {
				if (!d.player.isOnline()) continue;
				d.player.teleport(d.loc);
				check.add(d.player.getUniqueId());
			}
		}
	}
	
	/**
	 * проверка ожидающих прогрузки чанка
	 */
	private class WaitTP extends BukkitRunnable {
		@Override
		public void run() {
			ArrayList<Data> dat = new ArrayList<Data>(); //список для телепортации
			Iterator<Data> iter = wait.iterator();
			while(iter.hasNext()) {
				Data d = iter.next();
				if (d.loc.getChunk().isLoaded()) {
					//при обнаружении загруженного чанка игрок ставится в очередь на телепортацию
					dat.add(d);
					iter.remove();
				}
			}
			new JobTP(dat).runTask(Bukkit.getPluginManager().getPlugin("DupeFixer"));
			//добавление в список на проверку падения по таймеру
			ArrayList<Data> y = new ArrayList<Data>();
			long s = System.currentTimeMillis();
			for (Data d : dat) y.add(new Data(d.player, d.loc, s));
			ych.addAll(y);
		}
	}
	
	/**
	 * проверка на падение сквозь блоки
	 */
	private class YCheck extends TimerTask {
		@Override
		public void run() {
			ArrayList<Data> dat = new ArrayList<Data>(); //список для телепортации
			Iterator<Data> iter = ych.iterator();
			while(iter.hasNext()) {
				Data d = iter.next();
				if ((System.currentTimeMillis() - d.stamp) < main.conf.ytime) continue;
				if (!d.player.isOnline()) {
					iter.remove();
					continue;
				}
				if ((d.loc.getBlockY() - d.player.getLocation().getBlockY()) >= main.conf.yblocks)
					dat.add(new Data(d.player, d.loc));
				iter.remove();
			}
			new JobTP(dat).runTask(Bukkit.getPluginManager().getPlugin("DupeFixer"));
		}
	}
}