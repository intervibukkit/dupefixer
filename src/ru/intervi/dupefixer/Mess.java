package ru.intervi.dupefixer;

import java.io.File;

import org.bukkit.ChatColor;

import ru.intervi.littleconfig.ConfigLoader;
import ru.intervi.littleconfig.ConfigWriter;
import ru.intervi.littleconfig.ConfigLoader.IsArray;
import ru.intervi.littleconfig.utils.Utils;

public class Mess {
	Mess(Main m) {
		main = m;
		load();
	}
	private Main main;
	
	//Chars
	public String chdEntity = "null";
	public String chdEntityN = "null";
	public String chDrop = "null";
	public String chConsume = "null";
	public String chbBreak = "null";
	public String chInter = "null";
	public String chClickInv = "null";
	public String chClickInvN = "null";
	public String chPickup = "null";
	
	//Potions
	public String poDrop = "null";
	public String poConsume = "null";
	public String poInter = "null";
	
	//XRay
	public String xblock = "null";
	public String autoban = "null";
	public String sfblock = "null";
	public String sfore = "null";
	public String sforewn = "null";
	public String xfound = "null";
	
	//dupes
	//BigFlowers
	public String flInter = "null";
	//DropExit
	public String drPickup = "null";
	public String drQuick = "null";
	//FastClicks
	public String fcClickInv = "null";
	//HellPortal
	public String hpPortal = "null";
	public String hpPortalD = "null";
	//Lily
	public String lbPlace = "null";
	//NegativeItems
	public String niClickInv = "null";
	public String niOpenInv = "null";
	public String niDisp = "null";
	//EndPortal
	public String eportal = "null";
	
	//Main
	public String coms[] = {"null"};
	public String creload = "null";
	public String noperm = "null";
	public String memclean = "null";
	public String xstat[] = {"null"};
	public String nodata = "null";
	public String badname = "null";
	public String modoff = "null";
	public String uerror = "null";
	public String noupdate = "null";
	public String update = "null";
	public String noff = "null";
	public String non = "null";
	public String upstart = "null";
	public String erlog = "null";
	public String cgc = "null";
	
	//UpdateChecker
	public String ucerror = "null";
	
	//Spy
	public String spickup = "null";
	public String sdrop = "null";
	public String sclick = "null";
	
	//CMDUtils
	public String uuid = "null";
	public String offline = "null";
	public String name = "null";
	public String notfound = "null";
	public String firstp = "null";
	public String lastp = "null";
	
	//LotHopper
	public String hhalt = "null";
	public String hrep = "null";
	public String ltclear = "null";
	public String hfound = "null";
	
	//FarClick
	public String farclick = "null";
	
	//WhiteList
	public String whitelisted = "null";
	
	private String color(String str) {
		return ChatColor.translateAlternateColorCodes('&', str);
	}
	
	private String[] color(String arr[]) {
		String result[] = new String[arr.length];
		for (int i = 0; i < arr.length; i++) result[i] = color(arr[i]);
		return result;
	}
	
	void load() {
		//создание файлов и папок
		String sep = File.separator;
		String path = Utils.getFolderPath(this.getClass()) + sep + "DupeFixer" + sep + "langs";
		File dir = new File(path); //проверка папки
		if (!dir.isDirectory()) dir.mkdirs();
		File ru = new File((path + sep + "messages_ru.yml"));
		if (!ru.isFile()) Utils.saveFile(this.getClass().getResourceAsStream("/langs/messages_ru.yml"), ru);
		
		File lang = new File((path + sep + "messages_" + main.conf.lang + ".yml"));
		ConfigLoader config = null;
		try {
			config = new ConfigLoader(lang, false);
			//config.log = main.error; //назначение логгера
		} catch(Exception e) {e.printStackTrace();}
		if (config == null) return;
		
		if (main.conf.upgrade) { //обновление локализации
			try {
				File temp = File.createTempFile("DupeFixer", "messages.yml");
				Utils.saveFile(this.getClass().getResourceAsStream("/langs/messages_ru.yml"), temp);
				ConfigLoader tconfig = new ConfigLoader(temp);
				ConfigWriter writer = new ConfigWriter(lang);
				for (String name : tconfig.getOptionNames()) {
					if (!config.isSet(name)) {
						IsArray ia = tconfig.getMethods().checkArray(tconfig.getMethods().recIndexNoSection(name));
						if (ia.array) writer.setArray(name, tconfig.getStringArray(name), ia.skobka);
						else writer.setOption(name, tconfig.getString(name));
					}
				}
				writer.writeFile();
				temp.delete();
			} catch(Exception e) {e.printStackTrace();}
		}
		
		//загрузка значений
		chdEntity = color(config.getString("chdEntity"));
		chdEntityN = color(config.getString("chdEntityN"));
		chDrop = color(config.getString("chDrop"));
		chConsume = color(config.getString("chConsume"));
		chbBreak = color(config.getString("chbBreak"));
		chInter = color(config.getString("chInter"));
		chClickInv = color(config.getString("chClickInv"));
		chClickInvN = color(config.getString("chClickInvN"));
		chPickup = color(config.getString("chPickup"));
		poDrop = color(config.getString("poDrop"));
		poConsume = color(config.getString("poConsume"));
		poInter = color(config.getString("poInter"));
		xblock = color(config.getString("xblock"));
		autoban = color(config.getString("autoban"));
		flInter = color(config.getString("flInter"));
		drPickup = color(config.getString("drPickup"));
		drQuick = color(config.getString("drQuick"));
		fcClickInv = color(config.getString("fcClickInv"));
		hpPortal = color(config.getString("hpPortal"));
		hpPortalD = color(config.getString("hpPortalD"));
		lbPlace = color(config.getString("lbPlace"));
		niClickInv = color(config.getString("niClickInv"));
		niOpenInv = color(config.getString("niOpenInv"));
		niDisp = color(config.getString("niDisp"));
		coms = color(config.getStringArray("coms"));
		creload = color(config.getString("creload"));
		noperm = color(config.getString("noperm"));
		memclean = color(config.getString("memclean"));
		xstat = color(config.getStringArray("xstat"));
		nodata = color(config.getString("nodata"));
		badname = color(config.getString("badname"));
		modoff = color(config.getString("modoff"));
		uerror = color(config.getString("uerror"));
		noupdate = color(config.getString("noupdate"));
		update = color(config.getString("update"));
		noff = color(config.getString("noff"));
		non = color(config.getString("non"));
		sfblock = config.getString("sfblock");
		sfore = config.getString("sfore");
		sforewn = config.getString("sforewn");
		upstart = color(config.getString("upstart"));
		erlog = color(config.getString("erlog"));
		ucerror = config.getString("ucerror");
		spickup = color(config.getString("spickup"));
		sdrop = color(config.getString("sdrop"));
		sclick = color(config.getString("sclick"));
		uuid = color(config.getString("uuid"));
		offline = color(config.getString("offline"));
		name = color(config.getString("name"));
		notfound = color(config.getString("notfound"));
		hhalt = color(config.getString("hhalt"));
		hrep = color(config.getString("hrep"));
		ltclear = color(config.getString("ltclear"));
		hfound = color(config.getString("hfound"));
		xfound = color(config.getString("xfound"));
		farclick = color(config.getString("farclick"));
		cgc = color(config.getString("cgc"));
		firstp = color(config.getString("firstp"));
		lastp = color(config.getString("lastp"));
		eportal = color(config.getString("eportal"));
		whitelisted = color(config.getString("whitelisted"));
	}
}