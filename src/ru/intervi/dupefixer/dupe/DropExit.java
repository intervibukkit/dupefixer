package ru.intervi.dupefixer.dupe;

import java.util.concurrent.ConcurrentHashMap;

import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import ru.intervi.dupefixer.Main;

/**
 * фикс дюпа с выбросом вещи и выходом
 */
public class DropExit {
	public DropExit(Main m) {
		main = m;
	}
	
	private Main main;
	private ConcurrentHashMap<String, PickupData> pickups = new ConcurrentHashMap<String, PickupData>();
	
	/**
	 * очистить данные
	 */
	public void clear() {
		pickups.clear();
	}
	
	private class PickupData { //класс с данными для pickups
		PickupData(long t, Item i) {
			STAMP = t;
			ITEM = i;
		}
		
		final long STAMP; //время
		final Item ITEM; //предмет
	}
	
	public void onPickup(PlayerPickupItemEvent event) {
		if (!main.conf.enable || !main.conf.fastexit || !main.conf.dupefix) return;
		Player player = event.getPlayer();
		if (player.hasPermission("dupefixer.exemtp")) return;
		String name = player.getName();
		
		Item item = event.getItem();
		PickupData add = new PickupData(System.currentTimeMillis(), item);
		if (player.isOnline()) { //если игрок онлайн - добавление в БД
			if (pickups.containsKey(name)) pickups.replace(name, add); else pickups.put(name, add);
		} else { //если нет - убрать предмет
			pickups.remove(name);
			if (item.isValid()) item.remove();
			String cords = main.getLoc(player.getLocation());
			String itemname = item.getItemStack().getType().toString();
			if (main.conf.warn) player.sendMessage(main.conf.warnmess);
			main.info(main.repAll(main.mess.drPickup, "%name%", name, "%itemname%", itemname, "%cords%", cords));
		}
	}
	
	public void onQuit(PlayerQuitEvent event) {
		if (!main.conf.enable || !main.conf.fastexit || !main.conf.dupefix) {
			pickups.clear();
			return;
		}
		if (!main.conf.dupefix) return;
		Player player = event.getPlayer();
		if (player.hasPermission("dupefixer.exemtp")) return;
		String name = player.getName();
		
		long stamp = System.currentTimeMillis();
		PickupData data = pickups.get(name);
		if (data != null) {
			if ((stamp - data.STAMP) <= main.conf.fastexitms) { //если игрок слишком быстро вышел - удалить предмет
				if (data.ITEM.isValid()) data.ITEM.remove();
				String cords = main.getLoc(player.getLocation());
				String itemname = data.ITEM.getItemStack().getType().toString();
				if (main.conf.warn) player.sendMessage(main.conf.warnmess);
				main.info(main.repAll(main.mess.drQuick, "%name%", name, "%itemname%", itemname, "%cords%", cords));
			}
		}
		
		pickups.remove(name);
	}
}