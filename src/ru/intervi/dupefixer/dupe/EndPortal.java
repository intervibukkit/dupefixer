package ru.intervi.dupefixer.dupe;

import org.bukkit.event.entity.EntityPortalEvent;
import org.bukkit.entity.Item;
import org.bukkit.World.Environment;

import ru.intervi.dupefixer.Main;

/**
 * фикс дюпа с помощью портала в край
 */
public class EndPortal {
	public EndPortal(Main main) {
		this.main = main;
	}
	
	private Main main;
	
	public void onPortal(EntityPortalEvent event) {
		if (!main.conf.enable || !main.conf.dupefix || !main.conf.endportal) return;
		if (event.getTo() == null || event.getTo().getWorld() == null) return;
		if (!event.getTo().getWorld().getEnvironment().equals(Environment.THE_END)) return;
		String type = event.getEntity().getType().toString().toLowerCase();
		for (String s : main.conf.epallowed) {
			if (s.equals(type)) return;
		}
		if (main.conf.epitems && event.getEntity() instanceof Item) return;
		event.setCancelled(true); //отменяем телепорт
		main.info(main.repAll(main.mess.eportal, "%cords%", main.getLoc(event.getFrom())));
	}
}
