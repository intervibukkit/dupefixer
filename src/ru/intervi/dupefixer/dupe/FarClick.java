package ru.intervi.dupefixer.dupe;

import ru.intervi.dupefixer.Main;
import ru.intervi.dupefixer.Utils;

import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.entity.Player;
import org.bukkit.entity.HumanEntity;
import org.bukkit.Location;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.entity.EntityType;

/**
 * фикс дюпа вытаскиванием предмета из инвентаря в отгруженном чанке
 */
public class FarClick {
	public FarClick(Main m) {
		main = m;
	}
	
	private Main main;
	
	public void onClick(InventoryClickEvent event) {
		if (!main.conf.enable || !main.conf.dupefix || !main.conf.farclick) return;
		Inventory inv = event.getInventory();
		if (inv == null || inv.getType().equals(InventoryType.PLAYER)) return;
		Player player = (Player) event.getWhoClicked();
		if (player == null || !Utils.hasMethod(inv.getClass(), "getLocation")) return;
		Location iloc = null, ploc = null;
		try {
			iloc = inv.getLocation();
			ploc = player.getLocation();
		} catch(Exception e) {return;}
		if (iloc == null) return;
		//срабатывание если разные миры, превышено расстояние или чанк с инвентарем отгружен
		if (!iloc.getWorld().getName().equals(ploc.getWorld().getName()) || iloc.distance(ploc) > main.conf.fcblocks ||
				!iloc.getChunk().isLoaded()) {
			event.setCancelled(true);
			player.closeInventory();
			for (HumanEntity h : inv.getViewers()) {
				if (!h.getType().equals(EntityType.PLAYER)) continue;
				((Player) h).closeInventory();
			}
			main.info(main.repAll(main.mess.farclick, "%name%", player.getName(), "%ploc%", main.getLoc(ploc),
					"%iloc%", main.getLoc(iloc)));
		}
	}
}
