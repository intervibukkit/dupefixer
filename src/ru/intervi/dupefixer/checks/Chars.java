package ru.intervi.dupefixer.checks;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerItemConsumeEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

import ru.intervi.dupefixer.Main;

/**
 * проверка на нестандартные чары
 */
public class Chars {
	public Chars(Main m) {
		main = m;
	}
	private Main main;
	
	private boolean isBigChar(ItemStack item) { //является ли предмет нестандартно зачаренным
		if (item != null) {
			if (!item.getEnchantments().isEmpty()) { //есть ли чары
				Map<Enchantment, Integer> map = item.getEnchantments();
				Iterator<Map.Entry<Enchantment, Integer>> iter = map.entrySet().iterator();
				while (iter.hasNext()) { //проверка всех чар
					Map.Entry<Enchantment, Integer> entry = iter.next();
					int maxlvl = 0;
					//максимальное значение из конфига или по-умолчанию для чары
					if (main.conf.maxlevel > 0) maxlvl = main.conf.maxlevel; else maxlvl = entry.getKey().getMaxLevel();
					int lvl = entry.getValue().intValue();
					if (lvl > maxlvl | lvl < 0) { //больше или отрицательная чара - значит нестандартный зачар
						return true;
					}
				}
			}
		}
		return false;
	}
	
	public void damageEntity(EntityDamageByEntityEvent event) {
		if (main.conf.enable && main.conf.checks && main.conf.remchar) {
			Entity entity = event.getDamager();
			if (entity.getType().equals(EntityType.PLAYER)) {
				Player player = (Player) entity;
				if (player.hasPermission("dupefixer.exemtp")) return;
				PlayerInventory inv = player.getInventory();
				ItemStack items[] = {inv.getItemInMainHand(), inv.getItemInOffHand()};
				String name = player.getName();
				String cords = main.getLoc(player.getLocation());
				
				for (ItemStack item : items) {
					if (isBigChar(item)) { //проверка предмета в руке
						event.setCancelled(true);
						inv.remove(item);
						String itemname = item.getType().toString();
						if (main.conf.warn) player.sendMessage(main.conf.warnmess);
						main.info("у игрока " + name + " нашелся багнутый предмет " + itemname + " на " + cords + " (дюп)");
					}
				}
				
				//проверка брони
				ArrayList<String> itemsname = new ArrayList<String>();
				ItemStack[] plarmor = inv.getArmorContents();
				boolean del = false;
				if (plarmor != null) {
					for (int i = 0; i < plarmor.length; i++) {
						if (isBigChar(plarmor[i])) {
							del = true;
							itemsname.add(plarmor[i].getType().toString());
							inv.remove(plarmor[i]);
						}
					}
				}
				if (del) {
					if (main.conf.warn) player.sendMessage(main.conf.warnmess);
					String itemname = null;
					if (!itemsname.isEmpty()) { //втягивание массива названий предметов в строку
						itemname = "(" + itemsname.get(0);
						for (int i = 1; i < itemsname.size(); i++) itemname += ", " + itemsname.get(i);
						itemname += ")";
					}
					if (itemname != null) {
						main.info(main.repAll(main.mess.chdEntity, "%name%", name, "%itemname%", itemname, "%cords%", cords));
					} else {
						main.info(main.repAll(main.mess.chdEntityN, "%name%", name, "%cords%", cords));
					}
				}
			}
		}
	}
	
	public void onDrop(PlayerDropItemEvent event) {
		if (!main.conf.enable || !main.conf.checks || !main.conf.remchar) return;
		Player player = event.getPlayer();
		if (player.hasPermission("dupefixer.exemtp")) return;
		ItemStack item = event.getItemDrop().getItemStack();
		
		if (isBigChar(item)) {
			event.getItemDrop().remove();
			String itemname = item.getType().toString();
			String name = player.getName();
			String cords = main.getLoc(player.getLocation());
			if (main.conf.warn) player.sendMessage(main.conf.warnmess);
			main.info(main.repAll(main.mess.chDrop, "%name%", name, "%itemname%", itemname, "%cords%", cords));
		}
	}
	
	public void onConsume(PlayerItemConsumeEvent event) {
		if (!main.conf.enable || !main.conf.checks || !main.conf.remchar) return;
		Player player = event.getPlayer();
		if (player.hasPermission("dupefixer.exemtp")) return;
		ItemStack item = event.getItem();
		
		if (isBigChar(item)) {
			event.setCancelled(true);
			player.getInventory().remove(item);
			String itemname = item.getType().toString();
			String name = player.getName();
			String cords = main.getLoc(player.getLocation());
			if (main.conf.warn) player.sendMessage(main.conf.warnmess);
			main.info(main.repAll(main.mess.chConsume, "%name%", name, "%itemname%", itemname, "%cords%", cords));
		}
	}
	
	public void blockBreak(BlockBreakEvent event) {
		if (!main.conf.enable || !main.conf.checks || !main.conf.remchar) return;
		Player player = event.getPlayer();
		if (player.hasPermission("dupefixer.exemtp")) return;
		PlayerInventory inv = player.getInventory();
		ItemStack items[] = {inv.getItemInMainHand(), inv.getItemInOffHand()};
		
		for (ItemStack item : items) {
			if (isBigChar(item)) {
				event.setCancelled(true);
				inv.remove(item);
				String itemname = item.getType().toString();
				String cords = main.getLoc(event.getBlock().getLocation());
				String name = player.getName();
				if (main.conf.warn) player.sendMessage(main.conf.warnmess);
				main.info(main.repAll(main.mess.chbBreak, "%name%", name, "%itemname%", itemname, "%cords%", cords));
			}
		}
	}
	
	public void onInteract(PlayerInteractEvent event) {
		if (!main.conf.enable || !main.conf.checks || !main.conf.remchar) return;
		Player player = event.getPlayer();
		if (player.hasPermission("dupefixer.exemtp")) return;
		ItemStack item = event.getItem();
		
		if (isBigChar(item)) {
			event.setCancelled(true);
			player.getInventory().remove(item);
			String itemname = item.getType().toString();
			String cords = main.getLoc(player.getLocation());
			String name = player.getName();
			if (main.conf.warn) player.sendMessage(main.conf.warnmess);
			main.info(main.repAll(main.mess.chInter, "%name%", name, "%itemname%", itemname, "%cords%", cords));
		}
	}
	
	public void onClickInInv(InventoryClickEvent event) {
		if (!main.conf.enable || !main.conf.checks || !main.conf.remchar) return;
		Player player = (Player) event.getWhoClicked();
		if (player.hasPermission("dupefixer.exemtp")) return;
		
		ItemStack[] plinv = player.getInventory().getContents();
		ItemStack[] plarmor = player.getInventory().getArmorContents();
		ItemStack cursor = event.getCursor();
		boolean del = false;
		ArrayList<String> itemsname = new ArrayList<String>();
		if (plinv != null) { //проверка инвентаря
			for (int i = 0; i < plinv.length; i++) { //проверка инвентаря
				if (isBigChar(plinv[i])) {
					del = true;
					itemsname.add(plinv[i].getType().toString());
					player.getInventory().remove(plinv[i]);
				}
			}
		}
		if (plarmor != null) { //проверка брони
			for (int i = 0; i < plarmor.length; i++) { //проверка брони
				if (isBigChar(plarmor[i])) {
					del = true;
					itemsname.add(plarmor[i].getType().toString());
					player.getInventory().remove(plarmor[i]);
				}
			}
		}
		if (isBigChar(cursor)) { //проверка курсора
			del = true;
			itemsname.add(cursor.getType().toString());
			player.getInventory().remove(cursor);
		}
		
		if (del) { //отправка сообщения об удалении
			if (main.conf.warn) player.sendMessage(main.conf.warnmess);
			String itemname = null;
			String cords = main.getLoc(player.getLocation());
			String name = player.getName();
			if (!itemsname.isEmpty()) { //втягивание массива названий предметов в строку
				itemname = "(" + itemsname.get(0);
				for (int i = 1; i < itemsname.size(); i++) itemname += ", " + itemsname.get(i);
				itemname += ")";
			}
			if (itemname != null) {
				main.info(main.repAll(main.mess.chClickInv, "%name%", name, "%itemname%", itemname, "%cords%", cords));
			} else {
				main.info(main.repAll(main.mess.chClickInvN, "%name%", name, "%cords%", cords));
			}
		}
	}
	
	public void onPickup(PlayerPickupItemEvent event) {
		if (!main.conf.enable || !main.conf.checks || !main.conf.remchar) return;
		Player player = event.getPlayer();
		if (player.hasPermission("dupefixer.exemtp")) return;
		ItemStack item = event.getItem().getItemStack();
		if (isBigChar(item)) {
			event.setCancelled(true);
			event.getItem().remove();
			String itemname = item.getType().toString();
			String cords = main.getLoc(player.getLocation());
			String name = player.getName();
			if (main.conf.warn) player.sendMessage(main.conf.warnmess);
			main.info(main.repAll(main.mess.chPickup, "%name%", name, "%itemname%", itemname, "%cords%", cords));
		}
	}
}