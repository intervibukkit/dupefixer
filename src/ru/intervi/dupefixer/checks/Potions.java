package ru.intervi.dupefixer.checks;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerItemConsumeEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.PotionMeta;

import ru.intervi.dupefixer.Main;

/**
 * проверка на нестандартные зелья
 */
public class Potions {
	public Potions(Main m) {
		main = m;
	}
	private Main main;
	
	public void onDrop(PlayerDropItemEvent event) {
		if (!main.conf.enable || !main.conf.checks || !main.conf.rempotion) return;
		Player player = event.getPlayer();
		if (player.hasPermission("dupefixer.exemtp")) return;
		ItemStack item = event.getItemDrop().getItemStack();
		if (!item.getType().equals(Material.POTION)) return;
		
		if (item.hasItemMeta()) {
			PotionMeta meta = (PotionMeta) item.getItemMeta();
			if (meta.hasCustomEffects() || meta.hasEnchants()) {
				event.setCancelled(true);
				player.getInventory().remove(item);
				String name = player.getName();
				String cords = main.getLoc(player.getLocation());
				if (main.conf.warn) player.sendMessage(main.conf.warnmess);
				main.info(main.repAll(main.mess.poDrop, "%name%", name, "%cords%", cords));
			}
		}
	}
	
	public void onConsume(PlayerItemConsumeEvent event) {
		if (!main.conf.enable || !main.conf.checks || !main.conf.rempotion) return;
		Player player = event.getPlayer();
		if (player.hasPermission("dupefixer.exemtp")) return;
		ItemStack item = event.getItem();
		if (!item.getType().equals(Material.POTION)) return;
		
		if (item.hasItemMeta()) {
			PotionMeta meta = (PotionMeta) item.getItemMeta();
			if (meta.hasCustomEffects() || meta.hasEnchants()) {
				event.setCancelled(true);
				player.getInventory().remove(item);
				String name = player.getName();
				String cords = main.getLoc(player.getLocation());
				if (main.conf.warn) player.sendMessage(main.conf.warnmess);
				main.info(main.repAll(main.mess.poConsume, "%name%", name, "%cords%", cords));
			}
		}
	}
	
	public void onInteract(PlayerInteractEvent event) {
		if (!main.conf.enable || !main.conf.checks || !main.conf.rempotion) return;
		Player player = event.getPlayer();
		if (player.hasPermission("dupefixer.exemtp")) return;
		ItemStack item = event.getItem();
		
		if (item == null) return;
		Material type = item.getType();
		Action action = event.getAction();
		//фикс нестандартных зелий
		if ((action.equals(Action.RIGHT_CLICK_BLOCK) || action.equals(Action.RIGHT_CLICK_AIR)) && type.equals(Material.POTION)) {
			if (item.hasItemMeta()) {
				PotionMeta meta = (PotionMeta) item.getItemMeta();
				if (meta.hasCustomEffects() || meta.hasEnchants()) {
					event.setCancelled(true);
					player.getInventory().remove(item);
					String cords = main.getLoc(player.getLocation());
					String name = player.getName();
					if (main.conf.warn) player.sendMessage(main.conf.warnmess);
					main.info(main.repAll(main.mess.poInter, "%name%", name, "%cords%", cords));
				}
			}
		}
	}
}