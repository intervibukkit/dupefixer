package ru.intervi.dupefixer.dupe;

import org.bukkit.entity.EntityType;
import org.bukkit.entity.Horse;
import org.bukkit.entity.Horse.Variant;
import org.bukkit.event.entity.EntityPortalEvent;

import ru.intervi.dupefixer.Main;

/**
 * фикс дюпа с помощью портала в ад
 */
public class HellPortal {
	public HellPortal(Main m) {
		main = m;
	}
	private Main main;
	
	public void onPortal(EntityPortalEvent event) {
		if (main.conf.enable && main.conf.dupefix && main.conf.nportal) {
			EntityType type = event.getEntityType();
			String cords = main.getLoc(event.getFrom());
			if (type.equals(EntityType.MINECART_CHEST) || type.equals(EntityType.MINECART_HOPPER) || type.equals(EntityType.MINECART_FURNACE)) {
				event.setCancelled(true); //отменяем телепорт
				main.info(main.repAll(main.mess.hpPortal, "%cords%", cords));
			} else
			if (type.equals(EntityType.HORSE)) { //дюп с отправкой осла в портал
				Variant var = ((Horse) event.getEntity()).getVariant();
				if (var.equals(Variant.DONKEY) || var.equals(Variant.MULE)) {
					event.setCancelled(true); //отменяем телепорт
					main.info(main.repAll(main.mess.hpPortalD, "%cords%", cords));
				}
			}
		}
	}

}