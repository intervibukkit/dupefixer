package ru.intervi.dupefixer.bugs;

import org.bukkit.Achievement;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.World.Environment;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerTeleportEvent.TeleportCause;
import org.bukkit.Bukkit;

import ru.intervi.dupefixer.Main;

/**
 * предотвращение застревания в крае при заходе в портал
 */
public class AntiEndStuck {
	public AntiEndStuck(Main m) {
		main = m;
	}
	
	private Main main;
	
	public void onMove(PlayerMoveEvent event) {
		if (!main.conf.enable || !main.conf.bugs || !main.conf.antiend) return;
		Location to = event.getTo();
		if (event.getFrom().distance(to) >= 1) { //если игрок перешел на другой блок
			Player player = event.getPlayer();
			World world = to.getWorld();
			if (world.getEnvironment().equals(Environment.THE_END)) { //если измерение - край
				Block block = to.getBlock();
				if (block.getType().equals(Material.ENDER_PORTAL)) { //если игрок попал в портал
					event.setCancelled(true);
					if (player.hasAchievement(Achievement.END_PORTAL) && !player.hasAchievement(Achievement.THE_END)) {
						player.awardAchievement(Achievement.THE_END); //выдача ачивки
					}
					Location bed = player.getBedSpawnLocation();
					if (bed != null) player.teleport(bed, TeleportCause.UNKNOWN); //телепортация к кровати
					else { //телепортация на спаун
						String wname = world.getName();
						World normal = Bukkit.getWorld(wname.substring(0, wname.length() > 8 ? wname.length()-8 : wname.length()));
						if (normal != null) player.teleport(normal.getSpawnLocation(), TeleportCause.UNKNOWN);
						else player.teleport(world.getSpawnLocation(), TeleportCause.UNKNOWN);
					}
				}
			}
		}
	}
}