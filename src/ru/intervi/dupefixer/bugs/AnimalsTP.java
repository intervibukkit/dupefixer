package ru.intervi.dupefixer.bugs;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Tameable;
import org.bukkit.entity.Ocelot;
import org.bukkit.entity.Wolf;
import org.bukkit.entity.Player;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

import java.util.ArrayList;
import java.util.List;

import ru.intervi.dupefixer.Main;

/**
 * телепортер потерявшихся животных
 */
public class AnimalsTP {
	public AnimalsTP(Main m) {
		main = m;
	}
	
	private Main main;
	private BukkitTask task;
	
	public void startTimer() {
		task = new AnCheck().runTaskTimer(
				Bukkit.getPluginManager().getPlugin("DupeFixer"), main.conf.antpint, main.conf.antpint);
	}
	
	public void stopTimer() {
		if (task != null) {
			task.cancel();
			task = null;
		}
	}
	
	/**
	 * обработка энтитей по таймеру
	 */
	private class AnCheck extends BukkitRunnable {
		@Override
		public void run() {
			if (!main.conf.enable || !main.conf.bugs || !main.conf.animalstp) return;
			ArrayList<Data> list = new ArrayList<Data>();
			for (World world : Bukkit.getWorlds()) {
				for (Entity entity : world.getEntities()) {
					EntityType type = entity.getType();
					switch(type) {
					case OCELOT:
						if (((Ocelot) entity).isSitting()) continue; //сидячие животные пропускаются
						break;
					case WOLF:
						if (((Wolf) entity).isSitting()) continue;
						break;
					default:
						continue;
					}
					if (!((Tameable) entity).isTamed()) continue; //не прерученные пропускаются
					Player player = Bukkit.getPlayer(((Tameable) entity).getOwner().getUniqueId());
					if (player == null || !player.isOnline()) continue;
					Location pl = player.getLocation(),
							el = entity.getLocation();
					Material mat = pl.getBlock().getType();
					/*
					 * проверка, что игрок не застрял в блоках
					 * проверка дистнации и мира
					 */
					if ((mat.equals(Material.AIR) || mat.equals(Material.WATER) || mat.equals(Material.STATIONARY_WATER) ||
							mat.equals(Material.LONG_GRASS) || mat.equals(Material.DOUBLE_PLANT)) &&
							(!pl.getWorld().equals(el.getWorld()) || pl.distance(el) >= main.conf.antpdist))
						list.add(new Data(pl, entity));
				}
			}
			new AnTP(list).runTask(Bukkit.getPluginManager().getPlugin("DupeFixer"));
		}
	}
	
	private class Data { //данные для телепортации
		Data(Location l, Entity e) {
			LOCATION = l;
			ENTITY = e;
		}
		
		final Location LOCATION; //сущность
		final Entity ENTITY; //куда телепортировать
	}
	
	/**
	 * телепортер энтитей по списку
	 */
	private class AnTP extends BukkitRunnable {
		AnTP(List<Data> list) {
			this.list = list;
		}
		
		private List<Data> list;
		
		@Override
		public void run() {
			for (Data d : list) {
				if (!d.ENTITY.isValid()) continue;
				d.ENTITY.teleport(d.LOCATION);
			}
		}
	}
}