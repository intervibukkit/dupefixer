package ru.intervi.dupefixer.checks;

import org.bukkit.entity.Player;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.Material;
import org.bukkit.Bukkit;
import org.bukkit.BanList;
import org.bukkit.GameMode;
import org.bukkit.Location;

import ru.intervi.dupefixer.Main;
import ru.intervi.littleconfig.utils.EasyLogger;
import ru.intervi.littleconfig.utils.Utils;

import java.util.concurrent.ConcurrentHashMap;
import java.util.ArrayList;
import java.util.Map;
import java.util.UUID;
import java.util.Set;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Timer;
import java.util.TimerTask;
import java.io.File;

/**
 * выявление x-ray
 */
public class XRay {
	public XRay(Main m) {
		main = m;
		if (main.conf.enable && main.conf.checks && main.conf.xray) startTimer();
	}
	private Main main;
	
	//БД с данными игроков
	private ConcurrentHashMap<UUID, Data> p = new ConcurrentHashMap<UUID, Data>();
	private Timer timer = new Timer();
	
	private class Data { //контейнер с данными
		Data(Material mat, Location loc) { //конструктор с внесением данных
			put(mat, loc);
		}
		
		class MapData { //данные конкретной руды
			MapData() {updateTime();}
			
			double ore = 0; //добытое кол-во
			long time; //время добавления/обновления информации
			
			void updateTime() {time = System.currentTimeMillis();}
		}
		
		double norm = 0; //добытое кол-во обычной руды
		double ore = 0; //общее кол-во добытой руды
		long time = 0; //время последнего обновления
		boolean fh = false; //помогает ли копать друг (чем обусловлены нарушения)
		boolean ch = false; //был ли уличен в читерстве
		//БД с данными на руды
		ConcurrentHashMap<Material, MapData> map = new ConcurrentHashMap<Material, MapData>();
		//буфер локаций сломанных блоков не руды
		ArrayList<Location> list = new ArrayList<Location>();
		
		//высчитать процентное соотношение конкретной руды к общему кол-ву
		byte getPerOre(Material mat) {
			if (!map.containsKey(mat)) return 0;
			double d = map.get(mat).ore;
			return new Double(d*100/(ore+norm)).byteValue();
		}
		
		boolean isNorm(Material mat) { //является ли блок обычным (не рудой)
			if (isOre(mat)) return false;
			if (main.conf.nonorm) return true;
			for (int i = 0; i < main.conf.normal.length; i++) {
				if (mat.equals(main.conf.normal[i])) return true;
			}
			return false;
		}
		
		boolean isOre(Material mat) { //является ли блок рудой (поиск в списке)
			for (int i = 0; i < main.conf.ore.length; i++) {
				if (mat.equals(main.conf.ore[i])) return true;
			}
			return false;
		}
		
		boolean isBuffered(Location loc) { //проверить, есть ли ближайший блок в буфере
			UUID uuid = loc.getWorld().getUID();
			for(Location l : list) {
				if (uuid.equals(l.getWorld().getUID()) && loc.distance(l) == 1) return true;
			}
			return false;
		}
		
		void put(Material mat, Location loc) { //добавить новые данные
			if (isNorm(mat)) {
				norm++;
				if (main.conf.xbuffer) {
					while(list.size() >= main.conf.xbsize) list.remove(0); //обновление буфера
					list.add(loc);
				}
			}
			else if (isOre(mat)) {
				if (main.conf.xblock && ch) return; //предотвращение накрутки %, когда игрок упорно пытается ломать блок
				if (!isBuffered(loc)) return; //если собрано по стенам пещеры
				if (map.containsKey(mat)) {
					MapData md = map.get(mat);
					md.ore++;
					md.updateTime();
					map.replace(mat, md);
				} else {
					MapData md = new MapData();
					md.ore++;
					map.put(mat, md);
				}
				ore++;
			}
			time = System.currentTimeMillis();
		}
	}
	
	private class Cleaner extends TimerTask { //очистка базы
		@Override
		public void run() {
			if (p.isEmpty()) return;
			Iterator<Data> iter = p.values().iterator();
			while(iter.hasNext()) {
				Data dat = iter.next();
				long time = System.currentTimeMillis();
				if (((time-dat.time)/1000/60) > main.conf.xtime) iter.remove(); //чистка старых элементов
				else {
					if (dat.map.isEmpty()) {
						iter.remove();
						continue;
					}
					Iterator<Data.MapData> it = dat.map.values().iterator();
					while(it.hasNext()) { //чистка данных о блоках
						Data.MapData md = it.next();
						if (((time-md.time)/1000/60) > main.conf.xtime) it.remove();
					}
					if (dat.map.isEmpty()) iter.remove();
				}
			}
			//сохранение статистики
			if (!main.conf.xtest) return;
			tick++;
			if (tick >= main.conf.xtime) {
				tick = 0;
				if (!p.isEmpty()) saveStat();
			}
		}
		
		private int tick = 0;
		
		private void saveStat() { //запись статистики в файлы
			Iterator<UUID> iter = p.keySet().iterator();
			String path = Utils.getFolderPath(this.getClass()) + File.separator + "DupeFixer" + File.separator + "XRay";
			File fold = new File(path);
			if (!fold.isDirectory()) fold.mkdir();
			while(iter.hasNext()) {
				UUID uuid = iter.next();
				XStat stat = getStat(uuid);
				EasyLogger log = new EasyLogger("[XRay test]", new File((path + File.separator +
						Bukkit.getOfflinePlayer(uuid).getName() + ".txt")));
				log.offLog();
				log.info("-------------------------------------");
				log.info(main.repAll(main.mess.sfblock, "%norm0%", String.valueOf(stat.NORM[0]), "%norm1%", String.valueOf(stat.NORM[1])));
				log.info(main.repAll(main.mess.sfore, "%norm2%", String.valueOf(stat.NORM[2])));
				Iterator<Map.Entry<Material, double[]>> os = stat.SET.iterator();
				while(os.hasNext()) {
					Map.Entry<Material, double[]> entry = os.next();
					double st[] = entry.getValue();
					log.info(main.repAll(main.mess.sforewn, "%ore%", entry.getKey().toString(), "%st1%", String.valueOf(st[1]), "%st2%", String.valueOf(st[2])));
				}
			}
		}
	}
	
	public void startTimer() { //запуск таймера очистки
		timer = new Timer();
		timer.schedule(new Cleaner(), (main.conf.timclean*60*1000), (main.conf.timclean*60*1000));
	}
	
	public void stopTimer() { //остановка
		if (timer != null) {
			timer.cancel();
			timer = null;
		}
	}
	
	/**
	 * данные для внешнего запроса статистики
	 */
	public class XStat {
		XStat(Set<Map.Entry<Material, double[]>> set, double norm[], boolean cheat) {
			SET = set;
			NORM = norm;
			CHEATER = cheat;
		}
		
		/**
		 * список руд с данными
		 * 0 - время внесения данных
		 * 1 - добытое кол-во
		 * 2 - процентное соотношение к общему числу добытого
		 */
		public final Set<Map.Entry<Material, double[]>> SET;
		/**
		 * данные об обычных блоках (не руде)
		 * 0 - кол-во добытых обычных блоков
		 * 1 - процентное соотношение
		 * 2 - общее кол-во добытой руды
		 */
		public final double[] NORM;
		/**
		 * был ли игрок уличен в читерстве
		 */
		public final boolean CHEATER;
	}
	
	/**
	 * получить статистику по игроку
	 * @param uuid юид игрока
	 * @return статистика в виде XStat или null, если игрока в базе нет
	 */
	public XStat getStat(UUID uuid) {
		if (p.containsKey(uuid)) {
			Data dat = p.get(uuid);
			//обработка данных об обычных блоках
			double n[] = {dat.norm, (dat.norm/((dat.norm+dat.ore)/100)), dat.ore};
			HashMap<Material, double[]> m = new HashMap<Material, double[]>();
			Iterator<Map.Entry<Material, Data.MapData>> iter = dat.map.entrySet().iterator();
			while(iter.hasNext()) { //обработка данных о руде
				Map.Entry<Material, Data.MapData> entry = iter.next();
				Data.MapData md = entry.getValue();
				Material mat = entry.getKey();
				double d[] = { //создание массива с данными
						md.time,
						md.ore,
						dat.getPerOre(mat)
				};
				m.put(mat, d);
			}
			return new XStat(m.entrySet(), n, dat.ch);
		} else return null;
	}
	
	/**
	 * очистить данные
	 */
	public void clear() {
		p.clear();
	}
	
	//узнать максимальный процент соотношения руды с общим кол-вом добычи
	private byte getMaxPer(Material mat) {
		for (int i = 0; i < main.conf.ore.length; i++) {
			if (mat.equals(main.conf.ore[i])) {
				return main.conf.oreper[i];
			}
		}
		return -1;
	}
	
	private boolean isCheater(UUID uuid, Material mat) { //проверить на нарушение соотношения
		if (p.containsKey(uuid)) {
			Data dat = p.get(uuid);
			if ((dat.ore+dat.norm) <= main.conf.fblocks) return false; //если первые N блоков - сброс проверки
			if (dat.isOre(mat)) {
				byte per = dat.getPerOre(mat);
				byte dopper = getMaxPer(mat);
				if (dopper == -1) return false; //сброс при не нахождении
				if (per > dopper) return true; else return false; //если процент больше заданного - игрок читер
			} else return false;
		} else return false;
	}
	
	private boolean isHonest(UUID uuid) { //проверка на отсутствие нарушений
		if (p.containsKey(uuid)) {
			Data dat = p.get(uuid);
			//если игрок ничего не добывал - он не честный
			//т.к. метод используется для поиска друга-копателя
			if ((dat.ore+dat.norm) <= main.conf.fblocks) return false;
			if (dat.norm == 0 && dat.ore >= 0) return false;
			Iterator<Material> iter = dat.map.keySet().iterator();
			while(iter.hasNext()) { //проверка данных по всем рудам
				Material mat = iter.next();
				//поиск нарушения
				byte per = dat.getPerOre(mat);
				byte dopper = getMaxPer(mat);
				if (dopper == -1) continue;
				if (per > dopper) return false; //есть нарушение - не честный
			}
			return true; //нарушение не было найдено - честный
		} else return false;
	}
	
	private void put(UUID uuid, Material mat, Location loc) { //добавить или обновить данные в базе
		if (p.containsKey(uuid)) {
			Data dat = p.get(uuid);
			dat.put(mat, loc);
			p.replace(uuid, dat);
		} else {
			Data dat = new Data(mat, loc);
			p.put(uuid, dat);
		}
	}
	
	public void blockBreak(BlockBreakEvent event) {
		if (!main.conf.enable || !main.conf.checks || !main.conf.xray) return;
		Location bloc = event.getBlock().getLocation();
		if (main.conf.xminy != 0 && main.conf.xminy < bloc.getBlockY()) return; //проверка высоты
		Player player = event.getPlayer();
		if (player.hasPermission("dupefixer.exemtp")) return;
		if (player.getGameMode().equals(GameMode.CREATIVE)) return;
		UUID uuid = player.getUniqueId();
		Material mat = event.getBlock().getType();
		put(uuid, mat, bloc); //обновление данных
		if (main.conf.xtest) return; //сброс проверки при тестовом режиме
		Data dat = p.get(uuid);
		if (isCheater(uuid, mat)) { //если проверка на нарушение соотношения положительная
			if (main.conf.smartx) { //поиск друга-копателя в заданном радиусе
				Location loc = player.getLocation();
				for (Player pl : Bukkit.getOnlinePlayers()) {
					if (loc.getWorld().getName().equals(pl.getWorld().getName()) && loc.distance(pl.getLocation()) <= main.conf.xblocks) {
						//если рядом нашелся игрок
						UUID fu = pl.getUniqueId();
						if (p.containsKey(fu)) { //проверка игрока
							Data fd = p.get(fu);
							/*
							 * сброс наказания если игрок вскопал больше блоков и без нарушений
							 * что логично, если он вскапывает камень, а друг добывает руду
							 */
							if ((fd.ore+fd.norm) > (dat.ore+dat.norm) && isHonest(fu)) {
								if (!dat.fh) {
									dat.fh = true; //отметка о помощи, чтобы не было наказания когда друг уйдет
									dat.ch = false;
									p.replace(uuid, dat);
								}
								return;
							}
						}
					}
				}
			}
			
			if (dat.fh) { //сброс статистики, когда друг-копатель ушел
				p.remove(uuid);
				return;
			}
			
			String name = player.getName();
			String cords = main.getLoc(player.getLocation());
			byte per = dat.getPerOre(mat);
			byte dopper = getMaxPer(mat);
			boolean send = false;
			
			if (main.conf.xblock) { //блокировка вскапывания
				event.setCancelled(true);
				player.sendMessage(main.conf.xmess);
				if (!dat.ch) {
					main.info(main.repAll(main.mess.xblock, "%name%", name, "%cords%", cords, "%per%", String.valueOf(per),
							"%mat%", mat.toString(), "%norm%", String.valueOf(dat.norm), "%dopper%", String.valueOf(dopper)));
					send = true;
				}
			}
			
			//бан при сильном нарушении (превышение соотношения на N процентов)
			if (main.conf.autoban && per >= (dopper+main.conf.perexc)) {
				BanList ban = Bukkit.getBanList(BanList.Type.NAME);
				ban.addBan(name, main.conf.banmess, null, "DupeFixer");
				p.remove(uuid);
				main.info(main.repAll(main.mess.autoban, "%name%", name, "%cords%", cords, "%per%", String.valueOf(per),
						"%mat%", mat.toString(), "%norm%", String.valueOf(dat.norm), "%dopper%", String.valueOf(dopper)));
				send = true;
			}
			
			//отправка сообщения, если все блокировки отключены
			if (!send && !dat.ch) main.info(main.repAll(main.mess.xfound, "%name%", name, "%cords%", cords, "%per%", String.valueOf(per),
					"%mat%", mat.toString(), "%norm%", String.valueOf(dat.norm), "%dopper%", String.valueOf(dopper)));
			
			dat.ch = true; //отметка о чистерстве и защита от флуда
			p.replace(uuid, dat);
		} else {
			dat.ch = false;
			p.replace(uuid, dat);
		}
	}
}