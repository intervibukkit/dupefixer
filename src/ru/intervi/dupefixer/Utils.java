package ru.intervi.dupefixer;

import java.lang.reflect.Method;

/**
 * класс с общими методами
 */
public class Utils {
	/**
	 * проверка наличия метода в классе
	 * @param cls
	 * @param name
	 * @return true если есть
	 */
	public static boolean hasMethod(Class<?> cls, String name) {
		for (Method m : cls.getMethods())
			if (m.getName().equals(name)) return true;
		return false;
	}
}
