package ru.intervi.dupefixer;

import java.util.ArrayList;

import ru.intervi.bbcommits.APIChecker;
import ru.intervi.bbcommits.Commit;
import ru.intervi.bbcommits.exceptions.ParseErrorException;

import org.bukkit.command.CommandSender;

/**
 * проверка обновлений плагина
 */
public class Update extends Thread {
	private Main main;
	private CommandSender sender;
	Update(Main m, CommandSender s) {
		main = m;
		sender = s;
	}
	
	@Override
	public void run() {
		try {
			APIChecker ch = new APIChecker("InterVi", "dupefixer", "master");
			ch.parse();
			ArrayList<Commit> coms = ch.getCommits();
			if (coms.isEmpty()) sender.sendMessage(main.mess.uerror);
			else {
				//проверка по предыдущему комменту - если не совпадают, значит добавлен новый
				if (coms.get(1).NAME.equals(main.conf.commit)) sender.sendMessage(main.mess.noupdate);
				else sender.sendMessage(main.repAll(main.mess.update, "%link%", coms.get(0).LINK));
			}
		} catch(ParseErrorException e) {
			sender.sendMessage(main.mess.uerror);
			main.error(e.getException());
		} catch(Exception e) {
			sender.sendMessage(main.mess.uerror);
			main.error(e);
		}
	}
}