package ru.intervi.dupefixer.bugs;

import ru.intervi.dupefixer.Main;

import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.util.Vector;
import org.bukkit.event.player.PlayerTeleportEvent.TeleportCause;
import org.bukkit.entity.Player;
import org.bukkit.Location;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.entity.EntityType;
import org.bukkit.event.player.PlayerQuitEvent;

import java.util.HashMap;
import java.util.UUID;
import java.util.Date;

/**
 * убирание скорости при телепортации
 */
public class TPVelocity {
	public TPVelocity(Main m) {
		main = m;
	}
	
	private Main main;
	private HashMap<UUID, Date> map = new HashMap<UUID, Date>(); //те, кому нужно отменить урон от падения
	
	public void onTeleport(PlayerTeleportEvent event) {
		if (!main.conf.enable || !main.conf.bugs || !main.conf.tpvel) return;
		TeleportCause cause = event.getCause();
		if (!cause.equals(TeleportCause.COMMAND) && !cause.equals(TeleportCause.PLUGIN)) return; //пропуск не нужного
		if (main.conf.veldist != 0) { //пропуск ближних дистанций
			Location to = event.getTo(),
					from = event.getFrom();
			if (to.getWorld().getName().equals(from.getWorld().getName()) && to.distance(from) <= main.conf.veldist) return;
		}
		Player player = event.getPlayer();
		if (main.conf.yadd != 0) player.setVelocity(new Vector(0, main.conf.yadd, 0));
		player.setFallDistance(0);
		map.put(player.getUniqueId(), new Date());
	}
	
	public void onDamage(EntityDamageEvent event) { //чтобы гарантированно
		if (!main.conf.enable || !main.conf.bugs || !main.conf.tpvel) return;
		if (!event.getCause().equals(DamageCause.FALL) || !event.getEntityType().equals(EntityType.PLAYER)) return;
		UUID uuid = event.getEntity().getUniqueId();
		if (map.containsKey(uuid)) {
			if ((System.currentTimeMillis() / 1000) - (map.get(uuid).getTime() / 1000) <= main.conf.velint)
				event.setCancelled(true);
			map.remove(uuid);
		}
	}
	
	public void onQuit(PlayerQuitEvent event) { //очистка от вышедших
		if (!main.conf.enable || !main.conf.bugs || !main.conf.tpvel) return;
		map.remove(event.getPlayer().getUniqueId());
	}
	
	public void clear() {
		map.clear();
	}
}
