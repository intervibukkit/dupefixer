package ru.intervi.dupefixer.bugs;

import org.bukkit.event.block.BlockFadeEvent;
import org.bukkit.Material;
import org.bukkit.block.Block;

import ru.intervi.dupefixer.Main;

/**
 * блокировка потухания огня
 */
public class FireOut {
	public FireOut(Main m) {
		main = m;
	}
	
	private Main main;
	
	public void onFade(BlockFadeEvent event) {
		if (!main.conf.enable || !main.conf.bugs || !main.conf.nofade) return;
		Block block = event.getBlock();
		if (!block.getType().equals(Material.FIRE)) return;
		Block rblock = block.getRelative(0, -1, 0);
		if (block.getFace(rblock) == null) return;
		String rname = rblock.getType().toString();
		for (String name : main.conf.fadeblocks) {
			if (name.equalsIgnoreCase(rname)) {
				event.setCancelled(true);
				break;
			}
		}
	}
}
