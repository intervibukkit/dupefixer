package ru.intervi.dupefixer;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityPortalEvent;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.block.BlockDispenseEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerItemConsumeEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.inventory.InventoryMoveItemEvent;
import org.bukkit.event.block.BlockFadeEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;
import org.bukkit.event.player.PlayerLoginEvent;

import ru.intervi.dupefixer.dupe.*;
import ru.intervi.dupefixer.checks.*;
import ru.intervi.dupefixer.bugs.*;
import ru.intervi.dupefixer.spy.*;

public class Events implements Listener {
	Events(Main main) { //инициализация модулей
		this.main = main;
		hp = new HellPortal(main);
		ch = new Chars(main);
		ll = new Lily(main);
		po = new Potions(main);
		bf = new BigFlowers(main);
		fc = new FastClicks(main);
		ni = new NegativeItems(main);
		de = new DropExit(main);
		xr = new XRay(main);
		af = new AntiFall(main);
		as = new AntiEndStuck(main);
		sh = new Share(main);
		lt = new LotHopper(main);
		fo = new FireOut(main);
		tpv = new TPVelocity(main);
		fac = new FarClick(main);
		ep = new EndPortal(main);
		wh = new WhiteList(main);
	}
	
	private Main main;
	
	private HellPortal hp; //фикс дюпа с помощью портала в ад
	private Chars ch; //проверка чар
	private Lily ll; //фикс размножения больших цветков (НЕ дюп)
	private Potions po; //проверка зелий
	private BigFlowers bf; //фикс размножения больших цветков мукой (НЕ дюп)
	protected FastClicks fc; //предотвращение быстрых кликов
	private NegativeItems ni; //фикс быстрых кликов
	protected DropExit de; //фикс дюпа с выбрасыванием вещи и выходом
	protected XRay xr; //блокировка X-Ray
	protected AntiFall af; //предотвращение провала сквозь блоки
	private AntiEndStuck as; //предотвращение застревания в крае
	protected Share sh; //мониториг раздач
	protected LotHopper lt; //дюп замкнутыми воронками
	private FireOut fo; //блокировка потухания огня
	protected TPVelocity tpv; //убирание скорости при телепортации
	private FarClick fac; //фикс дюпа с отгруженным инвентарем
	private EndPortal ep; //фикс дюпа порталом края
	protected WhiteList wh; //фикс белого списка
	
	@EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
	public void onPortal(EntityPortalEvent event) {
		hp.onPortal(event);
		ep.onPortal(event);
	}
	
	@EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
	public void damageEntity(EntityDamageByEntityEvent event) {
		ch.damageEntity(event);
	}
	
	@EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
	public void onPlace(BlockPlaceEvent event) {
		ll.onPlace(event);
	}
	
	@EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
	public void onDrop(PlayerDropItemEvent event) {
		ch.onDrop(event);
		po.onDrop(event);
		sh.onDrop(event);
	}
	
	@EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
	public void onConsume(PlayerItemConsumeEvent event) {
		ch.onConsume(event);
		po.onConsume(event);
	}
	
	@EventHandler(priority = EventPriority.LOW, ignoreCancelled = true)
	public void blockBreak(BlockBreakEvent event) {
		ch.blockBreak(event);
		xr.blockBreak(event);
	}
	
	@EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
	public void onInteract(PlayerInteractEvent event) {
		ch.onInteract(event);
		po.onInteract(event);
		bf.onInteract(event);
	}
	
	@EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
	public void onClickInInv(InventoryClickEvent event) {
		fc.onClickInInv(event);
		ni.onClickInInv(event);
		ch.onClickInInv(event);
		sh.onClick(event);
		fac.onClick(event);
	}
	
	@EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
	public void onOpenInv(InventoryOpenEvent event) {
		ni.onOpenInv(event);
	}
	
	@EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
	public void onDisp(BlockDispenseEvent event) {
		ni.onDisp(event);
	}
	
	@EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
	public void onPickup(PlayerPickupItemEvent event) {
		de.onPickup(event);
		ch.onPickup(event);
		sh.onPickup(event);
	}
	
	@EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
	public void onQuit(PlayerQuitEvent event) {
		fc.onQuit(event);
		de.onQuit(event);
		sh.onQuit(event);
		tpv.onQuit(event);
	}
	
	@EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
	public void onJoin(PlayerJoinEvent event) {
		main.onJoin(event);
		af.onJoin(event);
	}
	
	@EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
	public void onTeleport(PlayerTeleportEvent event) {
		af.onTeleport(event);
	}
	
	@EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
	public void onTp(PlayerTeleportEvent event) {
		tpv.onTeleport(event);
	}
	
	@EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
	public void onMove(PlayerMoveEvent event) {
		as.onMove(event);
	}
	
	@EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
	public void onInv(InventoryMoveItemEvent event) {
		lt.onInv(event);
	}
	
	@EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
	public void onFade(BlockFadeEvent event) {
		fo.onFade(event);
	}
	
	@EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
	public void onDamage(EntityDamageEvent event) {
		tpv.onDamage(event);
	}
	
	@EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
	public void asyncPreLogin(AsyncPlayerPreLoginEvent event) {
		wh.onPreLogin(event);
	}
	
	@EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
	public void onLogin(PlayerLoginEvent event) {
		wh.onLogin(event);
	}
}