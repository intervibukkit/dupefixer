package ru.intervi.dupefixer.bugs;

import java.io.File;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent.Result;
import org.bukkit.event.player.PlayerLoginEvent;

import com.google.gson.Gson;

import ru.intervi.dupefixer.Main;

/**
 * фикс белого списка при online-mode=false
 */
public class WhiteList {
	public WhiteList(Main m) {
		main = m;
	}
	
	private Main main;
	private final String PATH = getPath() + File.separator + "whitelist.json";
	private final Charset ENCODING = Charset.defaultCharset();
	private final long last = 0; //время последнего редактирования
	private volatile HashSet<String> cache = new HashSet<String>(); //кэш ников из белого списка
	private volatile HashSet<UUID> set = new HashSet<UUID>(); //те, кого надо пропустить (буфер)
	
	private String getPath() { //получение пути к директории сервера
		String path = new File(this.getClass().getProtectionDomain().getCodeSource().getLocation().getPath()).getAbsolutePath();
		path = path.substring(0, path.lastIndexOf(File.separator));
		return path.substring(0, path.lastIndexOf(File.separator));
	}
	
	public class Data {
		public String name;
	}
	
	private void updateCache() { //обновление кэша ников, если файл перезаписан
		if (new File(PATH).lastModified() > last) {
			try {
				byte[] encoded = Files.readAllBytes(Paths.get(PATH));
				String json = new String(encoded, ENCODING);
				Gson gson = new Gson();
				cache.clear();
				for (Data d : gson.fromJson(json, Data[].class))
					cache.add(d.name);
			} catch(Exception e) {e.printStackTrace();}
		}
	}
	
	public void onPreLogin(AsyncPlayerPreLoginEvent event) { //проверка в отдельном потоке
		if (!main.conf.enable || !main.conf.bugs || !main.conf.fixwhitelist) return;
		if (Bukkit.getOnlineMode()) return;
		if (!event.getLoginResult().equals(Result.KICK_WHITELIST)) return;
		updateCache();
		if (cache.contains(event.getName())) set.add(event.getUniqueId());
	}
	
	public void onLogin(PlayerLoginEvent event) {
		if (!main.conf.enable || !main.conf.bugs || !main.conf.fixwhitelist) return;
		if (Bukkit.getOnlineMode()) return;
		if (!event.getResult().equals(org.bukkit.event.player.PlayerLoginEvent.Result.KICK_WHITELIST)) return;
		if (set.contains(event.getPlayer().getUniqueId())) { //пропуск игрока
			event.allow();
			set.remove(event.getPlayer().getUniqueId());
			main.getLogger().info(main.repAll(main.mess.whitelisted, "%name%", event.getPlayer().getName()));
		}
	}
	
	public void clear() {
		set.clear();
		cache.clear();
	}
}
