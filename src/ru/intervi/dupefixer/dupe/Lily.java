package ru.intervi.dupefixer.dupe;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.block.BlockPlaceEvent;

import ru.intervi.dupefixer.Main;

/**
 * фиккс визуального дюпа лилий
 */
public class Lily {
	public Lily(Main m) {
		main = m;
	}
	private Main main;
	
	public void onPlace(BlockPlaceEvent event) {
		if (!main.conf.enable || !main.conf.dupefix || !main.conf.boatwlily) return;
		Player player = event.getPlayer();
		if (player.hasPermission("dupefixer.exemtp")) return;
		Block block = event.getBlockPlaced();
		if (player.isInsideVehicle() && block.getType().equals(Material.WATER_LILY)) {
			event.setCancelled(true);
			event.setBuild(false);
			block.setType(Material.AIR);
			if (main.conf.duplily) {
				String name = player.getName();
				String cords = main.getLoc(player.getLocation());
				if (main.conf.warn) player.sendMessage(main.conf.warnmess);
				main.info(main.repAll(main.mess.lbPlace, "%name%", name, "%cords%", cords));
			}
		}
	}
}