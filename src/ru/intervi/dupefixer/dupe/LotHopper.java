package ru.intervi.dupefixer.dupe;

import org.bukkit.event.inventory.InventoryMoveItemEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.Location;
import org.bukkit.Material;

import java.util.concurrent.ConcurrentHashMap;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.Map.Entry;
import java.util.List;

import ru.intervi.dupefixer.Main;

/**
 * фикс дюпа воронками
 */
public class LotHopper {
	public LotHopper(Main m) {
		main = m;
	}
	
	private Main main;
	private ConcurrentHashMap<Location, Data> map = new ConcurrentHashMap<Location, Data>(); //воронки
	private ArrayList<Location> list = new ArrayList<Location>(); //воронки, при которых ивент надо блочить
	private ArrayList<Location> nosend = new ArrayList<Location>(); //для предотвращения флуда
	
	private class Data { //хранилище данных
		Data() {
			tick = 1;
			stamp = System.currentTimeMillis();
		}
		
		long tick;
		long stamp;
		
		void next() {
			tick++;
			stamp = System.currentTimeMillis();
		}
	}
	
	public void onInv(InventoryMoveItemEvent event) {
		if (!main.conf.enable || !main.conf.dupefix || !main.conf.lothopper) return;
		Inventory from = event.getSource(),
				to = event.getDestination();
		InventoryType ftype = from.getType(),
				ttype = to.getType();
		Location floc = from.getLocation(),
				tloc = to.getLocation();
		if (main.conf.movehalt) { //проверка на заблоченные воронки
			if ((ftype.equals(InventoryType.HOPPER) && list.contains(floc)) ||
					(ttype.equals(InventoryType.HOPPER) && list.contains(tloc))) {
				event.setCancelled(true);
				return;
			}
		}
		if (main.conf.chitem) { //только при перемещении заданного предмета
			String item = event.getItem().getType().toString();
			boolean r = true;
			for (String i : main.conf.mitems) {
				if (item.equalsIgnoreCase(i)) {
					r = false;
					break;
				}
			}
			if (r) return;
		}
		if (main.conf.eachother) { //только при схеме "воронка в воронку" (можно задействовать и сундуки)
			if (ftype.equals(InventoryType.HOPPER) && ttype.equals(InventoryType.HOPPER)) {
				if (map.containsKey(floc)) { //обновление данных
					Data d = map.get(floc);
					d.next();
					map.replace(floc, d);
				} else map.put(floc, new Data());
				if (map.containsKey(tloc)) {
					Data d = map.get(tloc);
					d.next();
					map.replace(tloc, d);
				} else map.put(tloc, new Data());
			}
			if (checkMap()) event.setCancelled(true); //проверка данных
			return;
		}
		if (!ftype.equals(InventoryType.HOPPER) && !ttype.equals(InventoryType.HOPPER)) return;
		Inventory inv = ftype.equals(InventoryType.HOPPER) ? from : to;
		Location loc = inv.getLocation();
		if (map.containsKey(loc)) { //обновления данных
			Data d = map.get(loc);
			d.next();
			map.replace(loc, d);
		} else map.put(loc, new Data());
		if (checkMap() && main.conf.movehalt) event.setCancelled(true); //проверка данных
	}
	
	private boolean checkMap() { //проверка данных и включение блокировок
		if (map.size() < main.conf.minhopper) return false;
		long time = System.currentTimeMillis();
		boolean result = false;
		HashMap<Location, List<Data>> near = new HashMap<Location, List<Data>>(); //группы воронок, которые рядом
		for (Entry<Location, Data> entry : map.entrySet()) { //составление списка групп воронок
			Location loc = entry.getKey();
			String wname = loc.getWorld().getName();
			ArrayList<Data> g = new ArrayList<Data>();
			for (Location loc2 : map.keySet()) {
				if (wname.equals(loc2.getWorld().getName()) && loc.distance(loc2) <= main.conf.hdist)
					g.add(entry.getValue());
			}
			near.put(loc, g);
		}
		for (Entry<Location, List<Data>> entry : near.entrySet()) { //обработка групп воронок
			Location loc = entry.getKey();
			int move = 0, hop = 0;
			ArrayList<Location> add = new ArrayList<Location>(); //для добавления в список блокировки
			for (Data d : entry.getValue()) {
				if ((time / 1000) - (d.stamp / 1000) <= main.conf.moveint) { //сбор данных и списка воронок
					move += d.tick;
					hop++;
					add.add(loc);
				} else map.remove(loc);
			}
			if (main.conf.eachother) move = move / 2; //действие по факту одно, но при данном параметре выходит 2
			if (move >= main.conf.maxmove && hop >= main.conf.minhopper) { //блокировки и уведомления
				String cords = main.getLoc(loc);
				if (main.conf.movehalt) {
					list.addAll(add);
					if (!result) main.info(main.repAll(main.mess.hhalt, "%loc%", cords));
				} else if (main.conf.hreplace) {
					for (Location locc : add) locc.getBlock().setType(Material.valueOf(main.conf.htype));
					if (!result) main.info(main.repAll(main.mess.hrep, "%loc%", cords, "%type%", main.conf.htype));
				} else {
					if (!nosend.contains(loc)) {
						main.info(main.repAll(main.mess.hfound, "%loc%", cords));
						nosend.add(loc);
					}
				}
				result = true;
			}
		}
		return result;
	}
	
	public void clear() {
		map.clear();
		list.clear();
		nosend.clear();
	}
	
	/**
	 * очистить список блокировки
	 */
	public void clearList() {
		list.clear();
	}
}