package ru.intervi.dupefixer.bugs;

import ru.intervi.dupefixer.Main;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

import java.util.List;

/**
 * отгрузка чанков
 */
public class ChunksGC {
	public ChunksGC(Main m) {
		main = m;
	}
	
	private Main main;
	private BukkitTask task;
	
	public void startTimer() {
		task = new GC().runTaskTimer(
				Bukkit.getPluginManager().getPlugin("DupeFixer"), main.conf.gcint, main.conf.gcint);
	}
	
	public void stopTimer() {
		if (task != null) {
			task.cancel();
			task = null;
		}
	}
	
	/**
	 * отгрузить чанки
	 * @return кол-во отгруженных чанков
	 */
	public long gc() {
		return new GC().gc();
	}
	
	private class GC extends BukkitRunnable { //отгрузчик чанков
		@Override
		public void run() {
			gc();
		}
		
		public long gc() {
			if (!main.conf.enable || !main.conf.bugs || !main.conf.chunksgc) return 0;
			int marginDist = (Bukkit.getViewDistance() * 16) + 16;
			long count = 0;
			for (World world : Bukkit.getWorlds()) { //обход миров
				Chunk chunks[] = world.getLoadedChunks();
				Location spawnLoc = world.getSpawnLocation();
				m: for (Chunk chunk : chunks) {
					if (!chunk.isLoaded()) continue;
					Location chunkLoc = chunk.getBlock(chunk.getX(), 0, chunk.getZ()).getLocation();
					//чанки рядом со спавном и игроками не отгружаются
					if (main.conf.keepspawn && chunkLoc.distance(spawnLoc) <= marginDist) continue;
					for (Player player : world.getPlayers()) {
						if (player == null || !player.isOnline()) continue;
						Location playerLoc = player.getLocation().getBlock().getLocation();
						playerLoc.setY(0);
						playerLoc.getBlock().getLocation().setY(0);
						if (chunkLoc.distance(playerLoc) <= marginDist) {
							continue m;
						}
					}
					if (world.unloadChunk(chunk.getX(), chunk.getZ(), true)) count++;
				}
			}
			if (main.conf.sysgc) {
				new Thread() {
					@Override
					public void run() {
						Runtime.getRuntime().gc();
					}
				}.start();
			}
			return count;
		}
	}
}
