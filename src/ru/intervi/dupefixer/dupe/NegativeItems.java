package ru.intervi.dupefixer.dupe;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Dispenser;
import org.bukkit.block.Dropper;
import org.bukkit.entity.Player;
import org.bukkit.event.block.BlockDispenseEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.Inventory;

import ru.intervi.dupefixer.Main;
import ru.intervi.dupefixer.Utils;

/**
 * фикс предметов с отрицательным количеством
 */
public class NegativeItems {
	public NegativeItems(Main m) {
		main = m;
	}
	private Main main;
	
	private List<String> check(Inventory inv) { //возвращает список удалённых багнутых предметов
		ArrayList<String> result = new ArrayList<String>();
		if (inv == null) return result;
		try {
			if (main.conf.rinoloc && (!Utils.hasMethod(inv.getClass(), "getLocation") || inv.getLocation() == null)) return result;
		} catch(Exception e) {return result;}
		if (main.conf.rinonames != null) {
			for (String name : main.conf.rinonames) {
				if (name.equalsIgnoreCase(inv.getTitle())) return result;
			}
		}
		for (ItemStack item : inv) {
			if (item == null || item.getType().equals(Material.AIR)) continue;
			int amount = item.getAmount();
			if (amount <= 0 || (main.conf.rembig && amount > item.getMaxStackSize())) {
				result.add(item.getType().toString());
				inv.remove(item);
			}
		}
		return result;
	}
	
	private void sendWarn(Player player, List<String> items, String mess) { //отправка сообщений
		String item = "(";
		for (String i : items) item += i + ", ";
		item = item.substring(0, item.length()-2) + ')';
		String cords = main.getLoc(player.getLocation());
		String name = player.getName();
		if (main.conf.warn) player.sendMessage(main.conf.warnmess);
		main.info(main.repAll(mess, "%name%", name, "%item%", item, "%cords%", cords));
	}
	
	public void onClickInInv(InventoryClickEvent event) {
		if (!main.conf.enable || !main.conf.dupefix || !main.conf.reminf) return;
		Player player = (Player) event.getWhoClicked();
		if (player.hasPermission("dupefixer.exemtp")) return;
		List<String> items = check(event.getInventory());
		if (!items.isEmpty()) {
			event.setCancelled(true);
			sendWarn(player, items, main.mess.niClickInv);
		}
	}
	
	public void onOpenInv(InventoryOpenEvent event) {
		if (!main.conf.enable || !main.conf.dupefix || !main.conf.reminf) return;
		Player player = (Player) event.getPlayer();
		if (player.hasPermission("dupefixer.exemtp")) return;
		List<String> items = check(event.getView().getTopInventory());
		if (!items.isEmpty()) {
			event.setCancelled(true);
			sendWarn(player, items, main.mess.niOpenInv);
		}
	}
	
	public void onDisp(BlockDispenseEvent event) {
		if (!main.conf.enable || !main.conf.dupefix || !main.conf.reminf) return;
		Block block = event.getBlock();
		Inventory inv = null;
		if (block.getState() instanceof Dispenser) inv = ((Dispenser) block.getState()).getInventory();
		else if (block.getState() instanceof Dropper) inv = ((Dropper) block.getState()).getInventory();
		else return;
		List<String> items = check(inv);
		if (!items.isEmpty()) {
			event.setCancelled(true);
			String cords = main.getLoc(block.getLocation());
			String item = "(";
			for (String i : items) item += i + ", ";
			item = item.substring(0, item.length()-2) + ')';
			main.info(main.repAll(main.mess.niDisp, "%item%", item, "%cords%", cords));
		}
	}
}