package ru.intervi.dupefixer.dupe;

import java.util.concurrent.ConcurrentHashMap;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType.SlotType;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;

import ru.intervi.dupefixer.Main;

/**
 * предотвращение быстрых кликов 
 */
public class FastClicks {
	public FastClicks(Main m) {
		main = m;
	}
	
	private Main main;
	
	/**
	 * БД с данными о кликах
	 * 0 - время
	 * 1 - номер слота
	 * 2 - кол-во кликов
	 * 3 - кол-во кликов в 1 слоте
	 */
	private ConcurrentHashMap<String, long[]> clicks = new ConcurrentHashMap<String, long[]>();
	
	/*
	 * очистить данные
	 */
	public void clear() {
		clicks.clear();
	}
	
	public void onClickInInv(InventoryClickEvent event) {
		if (!main.conf.enable || !main.conf.dupefix || !main.conf.fastclicks) return;
		
		Player player = (Player) event.getWhoClicked();
		if (player.hasPermission("dupefixer.exemtp")) return;
		ItemStack currentitem = event.getCurrentItem();
		
		if (currentitem != null) {
			if (!currentitem.getType().equals(Material.AIR)) {
				SlotType type = event.getSlotType();
				ClickType click = event.getClick();
				/**
				 * создание лимита на кол-во кликов в еденицу времени
				 */
				if (((click.isLeftClick() || click.isRightClick())) && (!type.equals(SlotType.CONTAINER) && !type.equals(SlotType.FUEL))) {
					String name = player.getName();
					long stamp = System.currentTimeMillis();
					int slot = event.getSlot();
					long c = 0, //кол-во кликов
							s = 0; //кол-во кликов в 1 слоту
					long data[] = clicks.get(name);
					if (data != null) {
						c = data[2]; s = data[3];
						//подсчет кликов в заданный промежуток
						if (((stamp / 1000) - (data[0] / 1000)) <= main.conf.maxtimecl) {
							c++;
							if (slot == data[1]) s++; else s = 0;
						} else c = 0;
						long add[] = {stamp, slot, c, s};
						clicks.replace(name, add);
					} else {
						long add[] = {stamp, slot, 1, 1};
						clicks.put(name, add);
					}
					if (c > main.conf.maxclicks) { //срабатывание, если лимит превышен
						//прерывание, если клики были в 1 слоте и не превысили спец лимит
						if (c >= s && s <= main.conf.maxsclicks && (c-s) <= main.conf.maxclicks) return;
						event.setCancelled(true);
						player.closeInventory();
						long add[] = {stamp, slot, 0, 0};
						clicks.replace(name, add);
						String cords = main.getLoc(player.getLocation());
						String itemname = currentitem.getType().toString();
						if (main.conf.warn) player.sendMessage(main.conf.warnmess);
						main.info(main.repAll(main.mess.fcClickInv, "%name%", name, "%itemname%", itemname, "%cords%", cords));
					}
				}
			}
		}
	}
	
	public void onQuit(PlayerQuitEvent event) {
		clicks.remove(event.getPlayer().getName()); //очистка БД
	}
}