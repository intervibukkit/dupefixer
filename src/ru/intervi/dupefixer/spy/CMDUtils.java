package ru.intervi.dupefixer.spy;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;

import java.util.UUID;
import java.util.Date;
import java.text.SimpleDateFormat;

import ru.intervi.dupefixer.Main;

/**
 * различные полезные утилиты
 */
public class CMDUtils {
	public CMDUtils(Main m) {
		main = m;
	}
	
	private Main main;
	
	/**
	 * получить UUID игрока
	 * @param name ник игрока
	 * @param sender отправитель команды на запрос
	 * @return форматированное сообщение
	 */
	public String getUUID(String name, CommandSender sender) {
		if (!sender.hasPermission("dupefixer.cmdutils")) return main.mess.noperm;
		Player player = Bukkit.getPlayerExact(name);
		if (player != null) return main.repAll(main.mess.uuid, "%uuid%", player.getUniqueId().toString());
		else return main.mess.offline;
	}
	
	/**
	 * получить ник игрока
	 * @param uuid UUID игрока в строковом виде
	 * @param sender отправитель команды на запрос
	 * @return форматированное сообщение
	 */
	public String getName(String uuid, CommandSender sender) {
		if (!sender.hasPermission("dupefixer.cmdutils")) return main.mess.noperm;
		OfflinePlayer player = Bukkit.getOfflinePlayer(UUID.fromString(uuid));
		if (player != null) return main.repAll(main.mess.name, "%name%", player.getName());
		else return main.mess.notfound;
	}
	
	/**
	 * получить даты первого и последнего захода, время онлайн
	 * @param uuid UUID игрока
	 * @return форматированное сообщение
	 */
	public String[] getOnline(UUID uuid) {
		OfflinePlayer player = Bukkit.getOfflinePlayer(uuid);
		long first = player.getFirstPlayed();
		long last = player.getLastPlayed();
		String[] result = new String[2];
		SimpleDateFormat format = new SimpleDateFormat(main.conf.dformat);
		result[0] = main.mess.firstp.replaceAll("%date%", format.format(new Date(first)));
		result[1] = main.mess.lastp.replaceAll("%date%", format.format(new Date(last)));
		return result;
	}
}