package ru.intervi.dupefixer;

import java.io.File;

import org.bukkit.ChatColor;
import org.bukkit.Material;

import ru.intervi.littleconfig.ConfigLoader;
import ru.intervi.littleconfig.ConfigLoader.IsArray;
import ru.intervi.littleconfig.ConfigWriter;
import ru.intervi.littleconfig.utils.Utils;

public class Config {
	private Main main;
	Config(Main m) {
		main = m;
		load();
	}
	
	String commit = "b0b40b8";
	
	//выключатели
	public boolean enable = true; //включить плагин
	public boolean dupefix = true; //включить фикс дюпов
	public boolean checks = true; //включить проверки
	public boolean bugs = true; //включить фикс багов
	public boolean spy = true; //режимы наблюдения
	public boolean duptocons = true; //выводить ли оповещения о попытках дюпа в консоль
	public boolean dupetogame = true; //выводить ли оповещения админам
	public boolean dupetolog = true; //записывать ли сообщения в логи
	
	public boolean dupepot = false; //выводить ли оповещения о попытках дюпа растений горшком (много флудит)
	//выводить ли оповещения о попытках визуального дюпа кувшинок (обычная игровая ситуация)
	public boolean duplily = false;
	
	public boolean warn = true; //уведомление игрокам о неудачных попытках
	public String warnmess = "&cНе пытайся дюпать!";
	
	public String lang = "ru"; //язык сообщений
	public boolean update = true; //проверять обновления
	public boolean upgrade = true; //обновлять конфиг
	
	//дюпы
	public boolean nportal = true; //дюп с отправкой грузовой вагонетки или осла в ад
	public boolean reminf = true; //удалять бесконечные предметы (багнутое количество)
		public boolean rinoloc = true; //не проверять инвентари без локации
		public String rinonames[] = {"gui"}; //не проверять инвентари с этими названиями
		public boolean rembig = false; //удалять ли при нестандартном кол-ве в большую сторону
	public boolean fastclicks = false; //дюп быстрыми кликами
		public int maxtimecl = 1; //промежуток времени, за который кликают
		public int maxclicks = 5; //максимальное кол-во кликов за назначенное кол-во секунды
		public int maxsclicks = 7; //максимальное кол-во кликов в одном слоту
	public boolean fastexit = true; //дюп быстрым поднятием предмета и выходом из игры
		public int fastexitms = 600; //таймаут между действиями в миллисекундах
	public boolean bigflower = true; //размножение больших цветков костной мукой
	public boolean boatwlily = true; //дюп кувшинок сидя в лодке
	public boolean lothopper = true; //дюп замкнутыми воронками
		public int moveint = 5; //интервал в секундах, за который ведется подсчет
		public int maxmove = 15; //макс. кол-во перемещений предмета
		public int minhopper = 2; //мин. кол-во воронок для срабатывания
		public int hdist = 50; //расстояние в блоках, в пределах которого группируются воронки
		public boolean movehalt = false; //блокировать перемещения
		public boolean eachother = true; //срабатывать только при схеме "воронка в воронку" (можно задействовать и сундуки)
		public boolean hreplace = true; //заменять воронки на другой блок
			public String htype = "DIRT"; //название блока
		public boolean chitem = true; //срабатывать только при перемещении заданного предмета
			public String mitems[] = {"DIAMOND", "DIAMOND_BLOCK", "DIAMOND_ORE"}; //предметы
	public boolean farclick = true; //дюп с открытым инвентарем в далекой вагонетке
		public int fcblocks = 10; //расстояние между игроком и инвентарем, после превышения которого нужно отменить клик
	public boolean endportal = true; //дюп порталом в край
		public boolean epitems = true; //разрешить вещам телепортацию
		public String epallowed[] = {"PLAYER"}; //список сущностей, которым можно телепортироватся
	
	//проверки
	public boolean remchar = true; //удалять предметы с нестандартным зачаром (обрабатывается во время кликов в инвентаре)
		public int maxlevel = 0; //максимальный уровень зачарования (если 0 - сравнивается со стандартным для данного предмета)
	public boolean rempotion = true; //удалять зелья с нестандартными характеристиками
	
	public boolean xray = true; //предотвращать использование x-ray
		public boolean xtest = false; //тестовый режим (только запись данных)
		public int xtime = 15; //буфер памяти по времени в минутах
			public int timclean = 1; //интервал очистки в минутах
		public int fblocks = 100; //не срабатывать первые %кол-во% блоков (-1 чтобы отключить)
		public Material normal[] = { //обычные блоки, вскапываемые в поисках руды
				Material.STONE, Material.COBBLESTONE, Material.GRAVEL, Material.DIRT,
				Material.GRASS, Material.SAND, Material.SANDSTONE
		};
		public Material ore[] = { //руда
				Material.DIAMOND_ORE, Material.GOLD_ORE, Material.IRON_ORE, Material.COAL_ORE,
				Material.REDSTONE_ORE, Material.LAPIS_ORE
		};
		public byte oreper[] = { //допустимый процент добычи из общего числа (в сравнении с обычными блоками)
				1, 1, 40, 60,
				20, 1
		};
		public boolean nonorm = false; //не использовать список обычных блоков (вся не руда разбавляет процент)
		public boolean autoban = false; //бан за x-ray
			public int perexc = 3; //порог превышения процента на кол-во процентов (например: в на процента, 20%+3=23%)
			public String banmess = "&c[Auto] x-ray"; //сообщение бана
		public boolean xblock = true; //блокировать добычу
			public String xmess = "&cНельзя использовать x-ray! Иди, добудь обычных блоков."; //сообщение предупреждения
		//не срабатывать, если рядом есть игроки с нормальной статистикой (один копает, другой добывает)
		public boolean smartx = true;
			public int xblocks = 120; //расстояние в блоках
		public boolean xbuffer = true; //буфер сломанных блоков не руды
			public int xbsize = 4; //размер буфера
		public int xminy = 0; //Y координата, от которой и ниже работает проверка (0 чтобы отключить)
			
	//баги
	public boolean animalstp = true; //телепортация потерявшихся животных
		public int antpint = 100; //таймер обхода в тиках
		public int antpdist = 30; //дистанция в блоках
	public boolean antifall = true; //предотвращение падений сквозь блоки
		public boolean onlycause = true; //обрабатывать тп только из списка причин
			public String tpcause[] = {"COMMAND", "PLUGIN"}; //список причин
		public boolean preload = true; //предзагрузка чанков
			public int plint = 10; //интервал таймера обработки списка в тиках
		public int falldist = 50; //расстояние между точками, при котором не надо действовать (0 чтобы отключить)
		public boolean checkair = true; //проверка на застревание в блоке
			public String[] airmat = {"AIR", "WATER"}; //пустые блоки
		public boolean timcheck = true; //проверять Y по таймеру после тп
			public int tcint = 1000; //интервал обработки списка
			public int ytime = 2000; //через сколько проверять игрока
			public int yblocks = 3; //после какого смещения по Y вниз реагировать (включительно)
		public boolean joincheck = true; //проверять на застревание при заходе на сервер
	public boolean antiend = true; //предотвращение застревания в крае при заходе в портал
	public boolean nofade = true; //блокировка потухания огня
		public String fadeblocks[] = {"NETHERRACK "}; //блоки, на которых огонь сверху не должен тухнуть
	public boolean tpvel = true; //убирать скорость при телепортации
		public double yadd = 0; //сколько прибавить к Y координате
		public int veldist = 5; //расстояние между точками, при котором не действовать
		public int velint = 2; //интервал в секундах, в течении которого после телепортации отменяется урон от падения
	public boolean chunksgc = true; //отгрузка чанков по таймеру
		public boolean keepspawn = true; //не отгружать спауны миров
		public int gcint = 600; //интервал отгрузки в тиках
		public boolean sysgc = true; //вызывать ли сборщик мусора
	public boolean fixwhitelist = true; //фикс белого списка при online-mode=false
	
	//наблюдение
	public boolean share = true; //мониторинг раздач
		public String sitems[] = {"DIAMOND"}; //список предметов
		public int samount = 64; //кол-во для реакции при раздаче и поднятии
		public int scamount = 128; //кол-во для реакции в инвентаре
		public int scwaitsec = 60; //пауза отслеживания данного игрока после срабатывания (антифлуд)
	
	//утилиты
	public String dformat = "YYYY-MM-dd/HH:mm:ss"; //формат вывода даты
		
	void load() { //чтение значений из конфига
		//создание конфига, если его нет
		String sep = File.separator;
		String path = Utils.getFolderPath(this.getClass()) + sep + "DupeFixer";
		File dir = new File(path); //проверка папки
		if (!dir.isDirectory()) dir.mkdirs();
		path += sep + "config.yml";
		File file = new File(path);
		if (!file.isFile()) Utils.saveFile(this.getClass().getResourceAsStream("/config.yml"), file);
		
		ConfigLoader config = null;
		try {
			config = new ConfigLoader(file, false);
			//config.log = main.error; //назначение логгера
		} catch(Exception e) {e.printStackTrace();}
		if (config == null) return;
		
		if (!config.isSet("upgrade") || config.getBoolean("upgrade")) { //обновление конфига
			try {
				File temp = File.createTempFile("DupeFixer", "config.yml");
				Utils.saveFile(this.getClass().getResourceAsStream("/config.yml"), temp);
				ConfigLoader tconfig = new ConfigLoader(temp);
				ConfigWriter writer = new ConfigWriter(file);
				for (String name : tconfig.getOptionNames()) {
					if (!config.isSet(name)) {
						IsArray ia = tconfig.getMethods().checkArray(tconfig.getMethods().recIndexNoSection(name));
						if (ia.array) writer.setArray(name, tconfig.getStringArray(name), ia.skobka);
						else writer.setOption(name, tconfig.getString(name));
					}
				}
				writer.writeFile();
				temp.delete();
			} catch(Exception e) {e.printStackTrace();}
		}
		
		//заполнение значений
		dupefix = config.getBoolean("dupefix");
		duptocons = config.getBoolean("duptocons");
		dupepot = config.getBoolean("dupepot");
		nportal = config.getBoolean("nportal");
		reminf = config.getBoolean("reminf");
		fastclicks = config.getBoolean("fastclicks");
		maxtimecl = config.getInt("maxtimecl");
		maxclicks = config.getInt("maxclicks");
		maxsclicks = config.getInt("maxsclicks");
		warn = config.getBoolean("warn");
		warnmess = ChatColor.translateAlternateColorCodes('&', config.getString("warnmess"));
		fastexit = config.getBoolean("fastexit");
		fastexitms = config.getInt("fastexitms");
		remchar = config.getBoolean("remchar");
		maxlevel = config.getInt("maxlevel");
		rempotion = config.getBoolean("rempotion");
		dupetogame = config.getBoolean("dupetogame");
		bigflower = config.getBoolean("bigflower");
		boatwlily = config.getBoolean("boatwlily");
		dupetolog = config.getBoolean("dupetolog");
		xray = config.getBoolean("xray");
		xtest = config.getBoolean("xtest");
		xtime = config.getInt("xtime");
		timclean = config.getInt("timclean");
		fblocks = config.getInt("fblocks");
		
		String smat[] = config.getStringArray("normal");
		Material mat[] = new Material[smat.length];
		for (int i = 0; i < smat.length; i++) {
			mat[i] = Material.valueOf(smat[i]);
		}
		normal = mat;
		
		String sore[] = config.getStringArray("ore");
		Material more[] = new Material[sore.length];
		byte pr[] = new byte[sore.length];
		for (int i = 0; i < sore.length; i++) {
			String val[] = sore[i].split(":");
			if (val.length <= 1) main.error("Config: wrong 'ore'");
			more[i] = Material.valueOf(val[0]);
			try {
				pr[i] = Byte.parseByte(val[1]);
			} catch(Exception e) {
				main.error("Config: wrong 'ore'");
				main.error(e);
			}
		}
		ore = more;
		oreper = pr;
		
		nonorm = config.getBoolean("nonorm");
		autoban = config.getBoolean("autoban");
		perexc = config.getInt("perexc");
		banmess = ChatColor.translateAlternateColorCodes('&', config.getString("banmess"));
		xblock = config.getBoolean("xblock");
		xmess = ChatColor.translateAlternateColorCodes('&', config.getString("xmess"));
		smartx = config.getBoolean("smartx");
		xblocks = config.getInt("xblocks");
		duplily = config.getBoolean("duplily");
		enable = config.getBoolean("enable");
		checks = config.getBoolean("checks");
		if (!enable) {
			dupefix = false;
			checks = false;
		}
		lang = config.getString("lang");
		update = config.getBoolean("update");
		bugs = config.getBoolean("bugs");
		animalstp = config.getBoolean("animalstp");
		antpint = config.getInt("antpint");
		antpdist = config.getInt("antpdist");
		antifall = config.getBoolean("antifall");
		onlycause = config.getBoolean("onlycause");
		tpcause = config.getStringArray("tpcause");
		preload = config.getBoolean("preload");
		plint = config.getInt("plint");
		checkair = config.getBoolean("checkair");
		airmat = config.getStringArray("airmat");
		timcheck = config.getBoolean("timcheck");
		tcint = config.getInt("tcint");
		ytime = config.getInt("ytime");
		yblocks = config.getInt("yblocks");
		joincheck = config.getBoolean("joincheck");
		antiend = config.getBoolean("antiend");
		spy = config.getBoolean("spy");
		share =  config.getBoolean("share");
		sitems = config.getStringArray("sitems");
		samount = config.getInt("samount");
		scamount = config.getInt("scamount");
		scwaitsec = config.getInt("scwaitsec");
		falldist = config.getInt("falldist");
		lothopper = config.getBoolean("lothopper");
		moveint = config.getInt("moveint");
		maxmove = config.getInt("maxmove");
		minhopper = config.getInt("minhopper");
		hdist = config.getInt("hdist");
		movehalt = config.getBoolean("movehalt");
		eachother = config.getBoolean("eachother");
		hreplace = config.getBoolean("hreplace");
		htype = config.getString("htype");
		nofade = config.getBoolean("nofade");
		fadeblocks = config.getStringArray("fadeblocks");
		tpvel = config.getBoolean("tpvel");
		yadd = config.getDouble("yadd");
		veldist = config.getInt("veldist");
		velint = config.getInt("velint");
		farclick = config.getBoolean("farclick");
		fcblocks = config.getInt("fcblocks");
		chunksgc = config.getBoolean("chunksgc");
		keepspawn = config.getBoolean("keepspawn");
		gcint = config.getInt("gcint");
		chitem = config.getBoolean("chitem");
		mitems = config.getStringArray("mitems");
		sysgc = config.getBoolean("sysgc");
		dformat = config.getString("dformat");
		xbuffer = config.getBoolean("xbuffer");
		xbsize = config.getInt("xbsize");
		xminy = config.getInt("xminy");
		endportal = config.getBoolean("endportal");
		epitems = config.getBoolean("epitems");
		epallowed = config.getStringArray("epallowed");
		for (int i = 0; i < epallowed.length; i++) epallowed[i] = epallowed[i].toLowerCase();
		rinoloc = config.getBoolean("rinoloc");
		rinonames = config.getStringArray("rinonames");
		fixwhitelist = config.getBoolean("fixwhitelist");
		rembig = config.getBoolean("rembig");
	}
}