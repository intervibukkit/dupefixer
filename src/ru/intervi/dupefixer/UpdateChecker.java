package ru.intervi.dupefixer;

import java.util.ArrayList;
import java.util.TimerTask;

import ru.intervi.bbcommits.APIChecker;
import ru.intervi.bbcommits.Commit;
import ru.intervi.bbcommits.exceptions.ParseErrorException;

/**
 * проверка обновлений для оповещений
 */
public class UpdateChecker extends TimerTask {
	Main main;
	UpdateChecker(Main m) {
		main = m;
	}
	
	@Override
	public void run() {
		try {
			APIChecker ch = new APIChecker("InterVi", "dupefixer", "master");
			ch.parse();
			ArrayList<Commit> coms = ch.getCommits();
			if (coms.isEmpty()) main.error(main.mess.ucerror);
			else {
				//проверка по предыдущему комменту - если не совпадают, значит добавлен новый
				if (!coms.get(1).NAME.equals(main.conf.commit)) {
					main.setUpdate(coms.get(0).LINK);
					this.cancel();
				}
			}
		} catch(ParseErrorException e) {
			main.error(e.getException());
		} catch(Exception e) {
			main.error(e);
		}
	}
}